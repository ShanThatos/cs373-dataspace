import pytest
from extensions import *

@setup_driver("/drugs")
def test_drugs_index(driver=None, url=None):
    try:
        element = WebDriverWait(driver, 20).until(
            EC.presence_of_element_located((By.CLASS_NAME, 'table'))
        )
    except Exception as e:
        print(e)
        assert False
    
    assert element.find_elements(by=By.TAG_NAME, value='th')[1].find_element(by=By.TAG_NAME, value='div').get_attribute("textContent") == 'Name'
    
    # check that first table result has loaded
    assert element.find_element(by=By.TAG_NAME, value='tbody').find_element(by=By.TAG_NAME, value='tr').find_elements(by=By.TAG_NAME, value='td')[1].find_element(by=By.TAG_NAME, value='span').get_attribute("textContent") == '3% SODIUM CHLORIDE'

    currentURL = driver.current_url
    assert currentURL == url

@setup_driver("/drugs")
def test_drugs_instance(driver=None, url=None):
    try:
        element = WebDriverWait(driver, 20).until(
            EC.presence_of_element_located((By.CLASS_NAME, 'table'))
        )
        element = element.find_element(by=By.TAG_NAME, value='tbody').find_element(by=By.TAG_NAME, value='tr').find_elements(by=By.TAG_NAME, value='td')[1].find_element(by=By.TAG_NAME, value='span').click()
        element = WebDriverWait(driver, 20).until(
            EC.presence_of_element_located((By.CLASS_NAME, 'sc-dkPtRN'))
        )
        assert element.get_attribute("textContent") == '3% SODIUM CHLORIDE'

    except Exception as e:
        print(e)
        assert False
