import pytest
from extensions import *


@setup_driver("/")
def testMainTextRender(driver=None, url=None):
    element = driver.find_element(by=By.CLASS_NAME, value='hero-container')
    assert element.find_element(by=By.TAG_NAME, value='h1').get_attribute("textContent") == 'PHARMABASE'
    assert element.find_element(by=By.TAG_NAME, value='p').get_attribute("textContent") == 'DEMYSTIFYING THE CHEMISTRY OF MODERN MEDICATIONS'
    currentURL = driver.current_url
    assert currentURL == url

@setup_driver("/")
def testDrugCard(driver=None, url=None):
    element = driver.find_element(by=By.CLASS_NAME, value='model-container').find_element(by=By.CLASS_NAME, value='justify-content-around').find_elements(by=By.CLASS_NAME, value='col-md-3')[0]
    assert element.find_element(by=By.CLASS_NAME, value='card-title').get_attribute("textContent") == 'Drugs'
    element.find_element(by=By.CSS_SELECTOR, value="a[href='/drugs']").send_keys("\n") # equivalent to pressing enter while focused on a specific element; click() was not working 
    
    try:
        element = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.TAG_NAME, 'h1'))
        )
    except Exception as e:
        print(e)
        assert False

    assert element.get_attribute("textContent") == 'Drugs Index'
    driver.back()
    currentURL = driver.current_url
    assert currentURL == url

@setup_driver("/")
def testElementsCard(driver=None, url=None):
    element = driver.find_element(by=By.CLASS_NAME, value='model-container').find_element(by=By.CLASS_NAME, value='justify-content-around').find_elements(by=By.CLASS_NAME, value='col-md-3')[1]
    assert element.find_element(by=By.CLASS_NAME, value='card-title').get_attribute("textContent") == 'Elements'
    element.find_element(by=By.CSS_SELECTOR, value="a[href='/elements']").send_keys("\n")
    
    try:
        element = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.TAG_NAME, 'h1'))
        )
    except Exception as e:
        print(e)
        assert False
    
    assert element.get_attribute("textContent") == 'Elements Index'
    driver.back()
    currentURL = driver.current_url
    assert currentURL == url

@setup_driver("/")
def testCompoundsCard(driver=None, url=None):
    element =  driver.find_element(by=By.CLASS_NAME, value='model-container').find_element(by=By.CLASS_NAME, value='justify-content-around').find_elements(by=By.CLASS_NAME, value='col-md-3')[2]
    assert element.find_element(by=By.CLASS_NAME, value='card-title').get_attribute("textContent") == 'Compounds'
    element.find_element(by=By.CSS_SELECTOR, value="a[href='/compounds']").send_keys("\n")
    
    try:
        element = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.TAG_NAME, 'h1'))
        )
    except Exception as e:
        print(e)
        assert False
    
    assert element.get_attribute("textContent") == 'Compounds Index'
    driver.back()
    currentURL = driver.current_url
    assert currentURL == url
