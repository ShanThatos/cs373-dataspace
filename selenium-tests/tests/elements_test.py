import pytest
from extensions import *

@setup_driver("/elements")
def test_elements_index(driver=None, url=None):
    try:
        element = WebDriverWait(driver, 20).until(
            EC.presence_of_element_located((By.CLASS_NAME, 'table'))
        )
    except Exception as e:
        print(e)
        assert False
    
    assert element.find_elements(by=By.TAG_NAME, value='th')[1].find_element(by=By.TAG_NAME, value='div').get_attribute("textContent") == 'Name'
    
    # check that first table result has loaded
    assert element.find_element(by=By.TAG_NAME, value='tbody').find_element(by=By.TAG_NAME, value='tr').find_elements(by=By.TAG_NAME, value='td')[1].find_element(by=By.TAG_NAME, value='span').get_attribute("textContent") == 'Aluminum'

    currentURL = driver.current_url
    assert currentURL == url

@setup_driver("/elements")
def test_elements_instance(driver=None, url=None):
    element = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.CLASS_NAME, 'table'))
    )
    element = element.find_element(by=By.TAG_NAME, value='tbody').find_element(by=By.TAG_NAME, value='tr').find_elements(by=By.TAG_NAME, value='td')[1].find_element(by=By.TAG_NAME, value='span').click()
    element = WebDriverWait(driver, 20).until(
        EC.presence_of_element_located((By.CLASS_NAME, 'sc-dkPtRN'))
    )
    assert element.get_attribute("textContent") == 'Aluminum'