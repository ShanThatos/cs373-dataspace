import os
import functools
from dotenv import load_dotenv
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options

load_dotenv()

BASE_URL = "https://www.pharmabase.me"


def setup_driver(url):
    def decorator(func):
        @functools.wraps(func)
        def wrapper():
            full_url = BASE_URL + url
            driver = getSeleniumChromeDriver()
            driver.get(full_url)
            try:
                func(driver=driver, url=full_url)
                driver.quit()
            except Exception as e:
                print(e)
                driver.quit()
                assert False
        return wrapper
    return decorator



def getSeleniumChromeDriver():
    if os.environ.get("USE_LOCAL_SELENIUM_DRIVER", "0") == "1":
        import chromedriver_binary
        chrome_options = Options()
        chrome_options.add_experimental_option('excludeSwitches', ['enable-logging'])
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--log-level=3")
        driver = webdriver.Chrome(options=chrome_options)
    else:
        chrome_options = Options()
        chrome_options.add_experimental_option('excludeSwitches', ['enable-logging'])
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--log-level=3")
        driver = webdriver.Remote(
            command_executor='http://selenium__standalone-chrome:4444/wd/hub',
            options=chrome_options)
    return driver