import unittest
import time
import sys
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
import sys
from extensions import BASE_URL, getSeleniumChromeDriver

URL = BASE_URL

class testSplash(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        # gets rid of 'Failed to send GpuChannelMsg_CreateCommandBuffer'
        #   by disabling GPU-based/assisted rendering and only using software based render.
        chrome_options.add_argument('--disable-gpu')
        cls.driver = getSeleniumChromeDriver()
        cls.driver.get(URL)
        
    @classmethod
    def tearDownClass(cls):
       #pass
       cls.driver.quit()

    def testMainTextRender(self):
        self.driver.get(URL)
        element = self.driver.find_element(by=By.CLASS_NAME, value='hero-container')
        assert element.find_element(by=By.TAG_NAME, value='h1').text == 'PHARMABASE'
        assert element.find_element(by=By.TAG_NAME, value='p').text == 'DEMYSTIFYING THE CHEMISTRY OF MODERN MEDICATIONS'
        currentURL = self.driver.current_url
        assert currentURL == URL

    def testDrugCard(self):
        element = self.driver.find_element(by=By.CLASS_NAME, value='model-container').find_element(by=By.CLASS_NAME, value='justify-content-around').find_elements(by=By.CLASS_NAME, value='col-md-3')[0]
        assert element.find_element(by=By.CLASS_NAME, value='card-title').text == 'Drugs'
        element.find_element(by=By.CSS_SELECTOR, value="a[href='/drugs']").send_keys("\n") # equivalent to pressing enter while focused on a specific element; click() was not working 
        element = self.driver.find_element(by=By.TAG_NAME, value='h1')
        assert element.text == 'Drugs Index'
        self.driver.back()
        currentURL = self.driver.current_url
        assert currentURL == URL


    def testElementsCard(self):
        element =  element = self.driver.find_element(by=By.CLASS_NAME, value='model-container').find_element(by=By.CLASS_NAME, value='justify-content-around').find_elements(by=By.CLASS_NAME, value='col-md-3')[1]
        assert element.find_element(by=By.CLASS_NAME, value='card-title').text == 'Elements'
        element.find_element(by=By.CSS_SELECTOR, value="a[href='/elements']").send_keys("\n")
        element = self.driver.find_element(by=By.TAG_NAME, value='h1')
        assert element.text == 'Elements Index'
        self.driver.back()
        currentURL = self.driver.current_url
        assert currentURL == URL
        

    def testCompoundsCard(self):
        element =  element = self.driver.find_element(by=By.CLASS_NAME, value='model-container').find_element(by=By.CLASS_NAME, value='justify-content-around').find_elements(by=By.CLASS_NAME, value='col-md-3')[2]
        assert element.find_element(by=By.CLASS_NAME, value='card-title').text == 'Compounds'
        element.find_element(by=By.CSS_SELECTOR, value="a[href='/compounds']").send_keys("\n")
        element = self.driver.find_element(by=By.TAG_NAME, value='h1')
        assert element.text == 'Compounds Index'
        self.driver.back()
        currentURL = self.driver.current_url
        assert currentURL == URL



if __name__ == "__main__":
    unittest.main()