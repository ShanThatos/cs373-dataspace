import unittest
import time
import sys
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
import sys
from extensions import BASE_URL, getSeleniumChromeDriver

URL = BASE_URL + "/drugs"


class testDrug(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        # gets rid of 'Failed to send GpuChannelMsg_CreateCommandBuffer'
        #   by disabling GPU-based/assisted rendering and only using software based render.
        chrome_options.add_argument('--disable-gpu')
        cls.driver = getSeleniumChromeDriver()
        cls.driver.get(URL)
        
    @classmethod
    def tearDownClass(cls):
       cls.driver.quit()

    def testDrugIndex(self):
        self.driver.get(URL)
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'table'))
            )
        except Exception as e:
            print(e)
            assert False
        
        assert element.find_element(by=By.TAG_NAME, value='th').text == 'Name'
        element = element.find_element(by=By.LINK_TEXT, value='METOLAZONE')

        currentURL = self.driver.current_url
        assert currentURL == URL

    def testDrugInstance(self):
        self.driver.get(URL)
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, 'table'))
            )
            element = element.find_element(by=By.LINK_TEXT, value='METOLAZONE').click()
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.TAG_NAME, 'h2'))
            )
            assert element.text == 'METOLAZONE'

        except Exception as e:
            print(e)
            assert False
    

if __name__ == "__main__":
    unittest.main()