import React, { useState }  from "react";
import { Navbar, Nav, Container, Form, FormControl, Button} from "react-bootstrap";
import logo from "../../images/pharmabase-logo.png";


const NavigationBar = () => {
  const [searchText, setSearchText] = useState("");

  const onTextChange = e => {
    e.preventDefault();
    console.log(e.target.value);
    setSearchText(e.target.value);
  };

  const onSubmitPress = e => {
    e.preventDefault();
    console.log(e.target.value);
    window.location.replace(`/search?q=${searchText}`);
  };

  return (
    <div>
      <Navbar bg="dark" variant="dark" expand="lg">
      <Container fluid>

        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll ">
          <Nav className="me-auto">

            <Navbar.Brand href="/"><img src={logo} width='200' height='30'/></Navbar.Brand>
          </Nav>
          <Nav
            className="my-2 my-lg-0"
            // style={{ maxHeight: "100px" }}
            navbarScroll
          >
            <Nav.Link href="/About">About</Nav.Link>
            <Nav.Link href="/drugs">Drugs</Nav.Link>
            <Nav.Link href="/compounds">Compounds</Nav.Link>
            <Nav.Link href="/elements">Elements</Nav.Link>
            <Nav.Link href="/vis/pharma">Our Visualizations</Nav.Link>
            <Nav.Link href="/vis/uni">Provider Visualizations</Nav.Link>
          <Form className="d-flex" onSubmit={onSubmitPress}>
            <FormControl
              variant='dark'
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
              onChange={onTextChange}
            />
            <a
              href={`/search?q=${searchText}`}
            >
              <Button 
                variant="outline-secondary" 
              >
                Search  
              </Button>
          </a>
          </Form>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>


    </div>
  );
};

export default NavigationBar;
