import { Vis1 } from "./Vis1";
import { Vis2 } from "./Vis2";
import { Vis3 } from "./Vis3";
import React from "react";


export const PharmaVis = () => {
    return (
        <div className="text-center pt-5">
            <Vis1 />
            <Vis3 />
            <Vis2 />
            
        </div>
    );
};