import React, { useEffect, useRef } from "react";
import * as d3 from "d3";
import o_data from "./data/vis1.json";

// pseudo-random color generator, varying the i parameter, varies the color
const getRandomColor = (d, i) => {
    let r1 = 49358367827;
    let r2 = 10293428574;
    let r3 = 89473539752;
    let x = 200;
    let m = 20;

    i++;
    let r = (i * r1) % x + m;
    let g = (i * r2) % x + m;
    let b = (i * r3) % x + m;
    return `rgb(${r}, ${g}, ${b})`;
};

const normalizeData = (data) => {
    let sum = d3.sum(data, d => d.count);
    data = data.map(d => ({ ...d, value: d.count / sum }));

    // squash all data entries that have a value < 5% into one data entry
    // yo github copilot is amazing
    // all the code it made below is actually correct, just a few minor changes to make it work better
    let min_threshold = 0.05;
    let min_count = 0;
    let min_sum = 0;
    let left_out_data = [];
    data = data.map(d => {
        if (d.value < min_threshold) {
            min_count++;
            min_sum += d.value;
            left_out_data.push(d);
            return { ...d, value: 0 };
        }
        return d;
    });
    if (min_count > 0) {
        data.push({
            name: "Other",
            symbol: "Other",
            extra: left_out_data.sort((a, b) => b.value - a.value),
            value: min_sum
        });
    }
    data = data.filter(d => d.value > 0);
    data = data.map((d, i) => ({ ...d, color: getRandomColor(d, i) }));
    return data;
};

const data = normalizeData(o_data);

export const Vis1 = () => {
    const svgRef = useRef();

    let otherslicebreakdown = data.filter(d => d.name === "Other")[0].extra.map(d => `${d.name} [${d.symbol}] (${(d.value * 100).toFixed(5)}%)`).join("\n");

    useEffect(() => {
        let margin = {top: 20, right: 20, bottom: 20, left: 20};
        let width = 500 - margin.left - margin.right;
        let height = 500 - margin.top - margin.bottom;

        let radius = Math.min(width, height) / 2;

        let svg = d3.select(svgRef.current)
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
        
        let pie = d3.pie()
            .value(d => d.value);
        let data_ready = pie(data);

        let arc_generator = d3.arc()
            .innerRadius(0)
            .outerRadius(radius);
        let arc_generator2 = d3.arc()
            .innerRadius(radius / 2)
            .outerRadius(radius);

        let arc = svg.selectAll("slices")
            .data(data_ready)
            .enter()
            .append("path")
            .attr("d", arc_generator)
            .attr("fill", d => d.data.color)
            .attr("stroke", "black")
            .style("stroke-width", "2px")
            .style("opacity", 0.7);

        arc.append("title")
            .text(d => `${d.data.name} [${d.data.symbol}] (${(d.data.value * 100).toFixed(1)}%)`);
        
        svg.selectAll("slices")
            .data(data_ready)
            .enter()
            .append("text")
            .attr("transform", d => `translate(${arc_generator2.centroid(d)})`)
            .attr("text-anchor", "middle")
            .attr("font-size", "15px")
            .text(d => `${d.data.symbol}\n${(d.data.value * 100).toFixed(1)}%`);
    }, []);

    return (
        <div className="m-3 border border-dark rounded border-4 p-3">
            <h2 className="text-center">Element Usage Amongst FDA Drugs</h2>
            <svg ref={svgRef} />
            <div className="row">
                <div className="col-6 d-flex flex-column">
                    <h5>Key</h5>
                    <div className="text-start p-2 mx-auto">
                        {data.map(d => (
                            <div key={d.symbol}>
                                <span style={{ backgroundColor: d.color, opacity: 0.7 }} className="px-2 me-1">&nbsp;</span>
                                {d.name}
                            </div>
                        ))}
                    </div>
                </div>
                <div className="col-6">
                    <h5>Other Slice</h5>
                    <textarea 
                        rows={8}
                        cols={30}
                        style={{ fontSize: "15px", whiteSpace: "pre-wrap" }} 
                        value={otherslicebreakdown}
                        readOnly
                    >
                    </textarea>
                </div>
            </div>
        </div>
    );
};