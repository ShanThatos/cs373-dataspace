import React, { useEffect, useRef } from "react";
import * as d3 from "d3";
import {RadarChart} from "../externalHelperCode/radarChart.js";

var data = require("./data/vis2.json")

export const Vis2 = () => {
    const svgRef = useRef();
    // pseudo-random color generator, varying the i parameter, varies the color
    const getRandomColor = (d, i) => {
        let r1 = 49358367827;
        let r2 = 10293428574;
        let r3 = 89473539752;
        let x = 200;
        let m = 20;

        i++;
        let r = (i * r1) % x + m;
        let g = (i * r2) % x + m;
        let b = (i * r3) % x + m;
        return `rgb(${r}, ${g}, ${b})`;
    };

    useEffect(() => {
        let margin = {top: 20, right: 110, bottom: 60, left: 110};
        let width = 800 - margin.left - margin.right;
        let height = 1000 - margin.top - margin.bottom;

        let svg = d3.select(svgRef.current)
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      
        var colors = ["#EDC951","#CC333F", "#00A0B0", "#05A105",]
        var color = d3.scaleOrdinal().range(colors);
            
        var radarChartOptions = {
            w: width,
            h: height,
            margin: margin,
            maxValue: 1,
            levels: 10,
            roundStrokes: true,
            color: color
        };
        //Call function to draw the Radar chart
        
        RadarChart(svgRef.current, data, radarChartOptions);

    }, []);

    return (
        <div className="m-3 border border-dark rounded border-4 p-3">
            <h2 className="text-center">Drugs Containing Active Compounds that Exhibit Properties Contributing to High Bioavailability</h2>
            <h6>Axes are metered in the percentage of drugs to exhibit that axis&apos; property (i.e. 100% on &quot;Mass &lt; 500.0 Da&quot; means all drugs exhibit that property)</h6>
            <svg ref={svgRef} />
            <div className="row">
                <div className="col-6 d-flex flex-column">
                    <h5>Key</h5>
                    <div className="text-start p-2 mx-auto">
                        
                        <div>
                            <span style={{ backgroundColor: "#EDC951", opacity: 0.7 }} className="px-2 me-1">&nbsp;</span>
                            {"Oral"}
                        </div>
                        <div>
                            <span style={{ backgroundColor: "#CC333F", opacity: 0.7 }} className="px-2 me-1">&nbsp;</span>
                            {"Intravenous"}
                        </div>
                        <div>
                            <span style={{ backgroundColor: "#00A0B0",  opacity: 0.7 }} className="px-2 me-1">&nbsp;</span>
                            {"Inhalation"}
                        </div>
                        <div>
                            <span style={{ backgroundColor: "#05A105", opacity: 0.7 }} className="px-2 me-1">&nbsp;</span>
                            {"Subcutaneous"}
                        </div>
                    </div>
                </div>

                <div className="col-6 d-flex flex-column">
                    <h5> Notes </h5>
                    <div className="text-start p-2 mx-auto">
                        <p>
                            <span style= {{fontWeight: "bold"}}> Bioavailability</span> {"is the measure of how much a drug is able to permeate through biological barriers to reach the target site. "}
                        </p>
                        <p>
                            <span style= {{color:"#CC333F", fontWeight: "bold"}}> Intravenous</span> {"drugs effectively have 100% bioavailability since the substance makes contact with blood circulation immediately."} 
                            <span style= {{color:"#EDC951", fontWeight: "bold"}}> Oral</span> {"drugs tend to have low bioavailability because the drug must be digested and absorbed through the intestinal tract to take effect."}
                            {"  The above 4 axes describe the desired chemical properties for a compound to exhibit high bioavailability. There are other desired properties not graphed here."}
                        </p>   
                        <p>
                            {" Notice that drugs with the "} <span style= {{color:"#EDC951", fontWeight: "bold"}}> oral</span> {" route of administration most proportionally contain compounds that meet all four conditions."}
                            {" Due to the low bioavailability of this ROA by nature, it is pertinent to meet as many of these conditions as possible to make the drug effective when swallowed."}

                        </p>
                          
                        <p>
                            <span style= {{textDecoration: "underline"}}>Citations:</span>
                            
                        </p>
                        <p> {" 1. Kim, J., &amp; De Jesus, O. (2022, February 17). Medication Routes of Administration. National Library of Medicine. Retrieved May 4, 2022, from https://www.ncbi.nlm.nih.gov/books/NBK568677/ "}  </p>
                        <p> {" 2.Lipinski's rule of five. Lipinski's rule of five | DrugBank Help Center. (n.d.). Retrieved May 4, 2022, from https://dev.drugbank.com/guides/terms/lipinski-s-rule-of-five  "}  </p>
                        
                    </div>
                </div>
            </div>
        </div>
    );
};