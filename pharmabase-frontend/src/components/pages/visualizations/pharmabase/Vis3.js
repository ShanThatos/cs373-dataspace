import React, { useEffect, useRef } from "react";
import * as d3 from "d3";
import data from "./data/vis3.json";


export const Vis3 = () => {
    const svgRef = useRef();
    //const data = [{"value": "01-01-2011"}, {"value": "01-01-1999"}];

    useEffect(() => {

        // set dimensions
        let margin = {top: 20, right: 30, bottom: 60, left: 90};
        let width = 800 - margin.left - margin.right;
        let height = 1000 - margin.top - margin.bottom;

        let parseDate = d3.timeParse("%d-%m-%Y");

        // append svg to the body of the page
        let svg = d3.select(svgRef.current)
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        
        // set range
        let xScale = d3.scaleTime()
                        .domain([new Date(1665, 6, 3), new Date(1940, 0, 1)])
                        .rangeRound([0, width]);
        let yScale = d3.scaleLinear()
                        .range([height, 0]);

        // set hist params
        let hist = d3.bin()
            .value(d => d.value)
            .domain(xScale.domain())
            .thresholds(xScale.ticks(18));

        // parse data for date only
        data.forEach(d => d.value = parseDate(d.value));

        // group data
        let bins = hist(data);

        yScale.domain([0, d3.max(bins, function(d) { return d.length; })]);

        // text helpers
        let yearFormat = d3.timeFormat("%Y");

        const build_disc = (num) => {
            return `${num} ${(num > 1? "discoveries": "discovery")}`;
        };

        // add rectangles
        svg.selectAll("rect")
            .data(bins)
          .enter().append("rect")
            .attr("class", "bar")
            .attr("x", 1)
            .attr("transform", function(d) {
                return "translate(" + xScale(d.x0) + "," + yScale(d.length) + ")"; })
            .attr("width", function(d) { return xScale(d.x1) - xScale(d.x0) -1 ; })
            .attr("height", function(d) { return height - yScale(d.length); })
            .attr("fill", getRandomColor)
            .append("title")
            .text(d => [`${yearFormat(d.x0)} ≤ year < ${yearFormat(d.x1)}`, build_disc(d.length)].join("\n"));

        // add the x Axis
        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(xScale));

        // add the Y Axis
        svg.append("g")
            .call(d3.axisLeft(yScale));

        // label axis
        svg.append("text")
            .attr("x", width / 2)
            .attr("y", height + 40)
            .style("text-anchor", "middle")
            .text("Year");

            svg.append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", -50)
            .attr("x", -height / 2)
            .attr("dy", "1em")
            .style("text-anchor", "middle")
            .text("Number of Elements Discovered");
        
        
    }, []);

    return (
        <div className="m-3 border border-dark rounded border-4 p-3">
            <h2 className="text-center">Year X Number of Elements Discovered Histogram</h2>
            <h5>Hover over a bar to view element discovery information per year range</h5>
            <svg ref={svgRef} />
        </div>
    );
};

const getRandomColor = (d, i) => {
    let r1 = 49358367827;
    let r2 = 10293428574;
    let r3 = 89473539752;
    let x = 200;
    let m = 20;

    i++;
    let r = (i * r1) % x + m;
    let g = (i * r2) % x + m;
    let b = (i * r3) % x + m;
    return `rgb(${r}, ${g}, ${b})`;
};