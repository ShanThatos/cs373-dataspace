import React, { useEffect, useRef } from "react";
import * as d3 from "d3";
import data from "./data/vis3.json";


export const Vis3 = () => {
    const svgRef = useRef();

    useEffect(() => {
        let margin = {top: 10, right: 30, bottom: 50, left: 60};
        let width = 800 - margin.left - margin.right;
        let height = 500 - margin.top - margin.bottom;

        let svg = d3.select(svgRef.current)
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        
        let xScale = d3.scaleLinear().domain([500, 1600]).range([0, width]);
        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(xScale));

        let yScale = d3.scaleLinear().domain([0, 100]).range([height, 0]);
        svg.append("g")
            .call(d3.axisLeft(yScale));
        
        svg.append("g")
            .selectAll("dot")
            .data(data)
            .enter()
            .append("circle")
            .attr("cx", d => xScale(d.satAvg))
            .attr("cy", d => yScale(d.admissionRate))
            .attr("r", 3)
            .style("fill", (d, i) => getRandomColor(d, i, 1))
            .append("title").text(d => `${d.name} - Admission Rate: ${d.admissionRate}% - SAT Average: ${d.satAvg}`);
        
        svg.append("text")
            .attr("x", width / 2)
            .attr("y", height + 40)
            .style("text-anchor", "middle")
            .text("SAT Average");
        
        svg.append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", -50)
            .attr("x", -height / 2)
            .attr("dy", "1em")
            .style("text-anchor", "middle")
            .text("Admission Rate");
    }, []);

    return (
        <div className="m-3 border border-dark rounded border-4 p-3">
            <h2 className="text-center">University SAT Average X Admission Rates Scatterplot</h2>
            <h5>Hover over a dot to view the university</h5>
            <svg ref={svgRef} />
        </div>
    );
};

const getRandomColor = (d, i, mul=1) => {
    let r1 = 49358367827;
    let r2 = 10293428574;
    let r3 = 89473539752;
    let x = 200;
    let m = 20;

    i++;
    let r = (i * r1) % x + m;
    let g = (i * r2) % x + m;
    let b = (i * r3) % x + m;
    r = Math.min(Math.round(r * mul), 255);
    g = Math.min(Math.round(g * mul), 255);
    b = Math.min(Math.round(b * mul), 255);
    return `rgb(${r}, ${g}, ${b})`;
};