import React, { useEffect, useRef } from "react";
import * as d3 from "d3";
import data from "./data/vis1.json";


export const Vis1 = () => {
    const svgRef = useRef();

    useEffect(() => {
        let margin = {top: 20, right: 30, bottom: 60, left: 90};
        let width = 800 - margin.left - margin.right;
        let height = 1000 - margin.top - margin.bottom;

        let svg = d3.select(svgRef.current)
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        
        let xScale = d3.scaleLinear().domain([0, d3.max(data, x => x.value) * 11 / 10]).range([0, width]);
        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(xScale).tickFormat((d) => `$${d}`))
            .selectAll("text")
            .attr("transform", "translate(-10,0)rotate(-45)")
            .style("text-anchor", "end")
            .style("font-size", "15px");
        
        let yScale = d3.scaleBand().domain(data.map(y => y.label)).range([0, height]).padding(0.5);
        svg.append("g").call(d3.axisLeft(yScale))
            .style("font-size", "15px");

        svg.selectAll("rect")
            .data(data)
            .enter()
            .append("rect")
            .attr("x", xScale(0))
            .attr("y", d => yScale(d.label))
            .attr("width", d => xScale(d.value))
            .attr("height", yScale.bandwidth())
            .attr("fill", getRandomColor)
            .append("title").text(d => `${d.label} - $${d.value.toFixed(2)}`);
    }, []);

    return (
        <div className="m-3 border border-dark rounded border-4 p-3">
            <h2 className="text-center">Average College Cost Per State</h2>
            <h5>Hover over a bar to view the average cost</h5>
            <svg ref={svgRef} />
        </div>
    );
};


const getRandomColor = (d, i) => {
    let r1 = 49358367827;
    let r2 = 10293428574;
    let r3 = 89473539752;
    let x = 200;
    let m = 20;

    i++;
    let r = (i * r1) % x + m;
    let g = (i * r2) % x + m;
    let b = (i * r3) % x + m;
    return `rgb(${r}, ${g}, ${b})`;
};