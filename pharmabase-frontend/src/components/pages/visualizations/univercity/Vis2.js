import React, { useEffect, useRef } from "react";
import * as d3 from "d3";
import o_data from "./data/vis2.json";

const truncate = (str, n) => (str.length > n) ? str.substr(0, n-1) + "..." : str;

const calcRadius = d => Math.pow(d.value / 1000000000.0, 0.6) + 10;
const getRandomColor = (d, i, mul=1) => {
    let r1 = 49358367827;
    let r2 = 10293428574;
    let r3 = 89473539752;
    let x = 200;
    let m = 20;

    i++;
    let r = (i * r1) % x + m;
    let g = (i * r2) % x + m;
    let b = (i * r3) % x + m;
    r = Math.min(Math.round(r * mul), 255);
    g = Math.min(Math.round(g * mul), 255);
    b = Math.min(Math.round(b * mul), 255);
    return `rgb(${r}, ${g}, ${b})`;
};
const data = o_data.map((d, i) => ({...d, radius: calcRadius(d), fillColor: getRandomColor(d, i), borderColor: getRandomColor(d, i, 0.9)}));

export const Vis2 = () => {
    const svgRef = useRef();

    useEffect(() => {
        
        let width = 1000;
        let height = 1000;

        let svg = d3.select(svgRef.current)
            .attr("width", width)
            .attr("height", height);

        let node = svg.append("g")
            .selectAll("circle")
            .data(data)
            .enter()
            .append("g")
            .attr("transform", d => `translate(${width / 2 - d.radius}, ${height / 2 - d.radius})`);
        
        node.append("circle")
            .attr("r", d => d.radius)
            .attr("cx", d => d.radius)
            .attr("cy", d => d.radius)
            .style("fill", d => d.fillColor)
            .style("fill-opacity", 0.3)
            .attr("stroke", d => d.borderColor)
            .style("stroke-width", 4);
        
        node.append("image")
            .attr("xlink:href", d => d.logo)
            .attr("x", d => d.radius / 2)
            .attr("y", d => d.radius / 3.5)
            .attr("width", d => d.radius)
            .attr("height", d => d.radius);
        
        node.append("text")
            .attr("x", d => d.radius)
            .attr("y", d => d.radius * 1.4)
            .attr("text-anchor", "middle")
            .attr("dominant-baseline", "central")
            .attr("font-size", d => d.radius / 3.5)
            .attr("font-weight", "bold")
            .text(d => truncate(d.label, 10));
        
        node.append("title").text(d => `${d.label} - $${(d.value / 1000000).toFixed(2)}M`);

        let simulation = d3.forceSimulation()
            .force("center", d3.forceCenter().x(width / 2).y(height / 2))
            .force("charge", d3.forceManyBody().strength(.1))
            .force("collide", d3.forceCollide().strength(1).radius(d => calcRadius(d) + 2).iterations(1));

        simulation.nodes(data)
            .on("tick", () => {
                node.attr("transform", d => `translate(${d.x - d.radius}, ${d.y - d.radius})`);
            });

    }, []);

    return (
        <div className="m-3 border border-dark rounded border-4 p-3">
            <h2 className="text-center">Relative Company Annual Revenue Bubble Chart</h2>
            <h5>Hover over a company to view their annual revenue</h5>
            <svg ref={svgRef} />
        </div>
    );
};