import { Vis1 } from "./Vis1";
import { Vis2 } from "./Vis2";
import { Vis3 } from "./Vis3";
import React from "react";


export const UniVis = () => {
    return (
        <div className="text-center pt-5">
            <Vis1 />
            <Vis2 />
            <Vis3 />
        </div>
    );
};