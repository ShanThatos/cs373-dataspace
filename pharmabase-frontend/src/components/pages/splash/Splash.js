import React from "react";
import { Row} from "react-bootstrap";
import HeroSection from "./HeroSection";
import ModelCard from "./ModelCard";
import "../../../styles/ModelSection.css";

import drugPicture from "../../../images/icons/medicine.png";
import elementPicture from "../../../images/icons/atom.png";
import compoundPicture from "../../../images/icons/compound.png";

const drugsInfo = {
  name: "Drugs",
  numEntries: -1,
  picture: drugPicture,
  link: "/drugs",
  description: "Pharmaceutical medicines and treatments used for various illnesses and ailments"
};

const elementsInfo = {
  name: "Elements",
  numEntries: -1,
  picture: elementPicture,
  link: "/elements",
  description: "Chemical substances in their most basic form that make up our universe."
};

const compoundsInfo = {
  name: "Compounds",
  numEntries: -1,
  picture: compoundPicture,
  link: "/compounds",
  description: "Chemical compounds used in the creation of our drugs and medicines."
};


const Splash = () => {
  return (
    <div>
      <HeroSection/>
      <div className="model-container">
        <Row className="justify-content-around my-5 g-0">
          <ModelCard {...drugsInfo} />
          <ModelCard {...elementsInfo} />
          <ModelCard {...compoundsInfo} />
        </Row>
      </div>
      <div className="p-5 bg-dark">
        <iframe width="873" height="491" src="https://www.youtube.com/embed/EsvgmoQQlNE" title="YouTube video player" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
      </div>
    </div>
  );
};


export default Splash;
