import React from "react";
import "../../../styles/HeroSection.css";



function HeroSection() {
    return (
        <div className="hero-container">
          <h1>PHARMABASE</h1>
          <p>DEMYSTIFYING THE CHEMISTRY OF MODERN MEDICATIONS</p>
        </div>
    );
}

export default HeroSection;
