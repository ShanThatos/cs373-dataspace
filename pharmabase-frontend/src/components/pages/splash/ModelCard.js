import PropTypes from "prop-types";
import React from "react";
import { Card, Col } from "react-bootstrap";

const ModelCard = (props) => {
  return (
    <Col className="col-md-3">
      <a href={props.link} style ={{ textDecoration: "none" }}>
        <Card className="m-3" style={{ minWidth: "250px"}} bg={"secondary"} text={"light"} >
          <Card.Img variant="top" className="mx-auto" style = {{width: "250px"}}src={props.picture} />

          <Card.Body>
          <Card.Title>{props.name}</Card.Title>

          <hr/>

          <Card.Text>{props.description}</Card.Text>

          </Card.Body>
        </Card>
      </a>
      {/* <Card.Link href= {props.link}  style ={{ textDecoration: "none" }}>

      </Card.Link> */}
    </Col>
  );
};

ModelCard.propTypes = {
  name: PropTypes.string.isRequired,
  picture: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
};


export default ModelCard;
