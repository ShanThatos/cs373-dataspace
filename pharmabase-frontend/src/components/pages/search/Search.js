import React, { useEffect, useState } from "react";
import {useSearchParams} from "react-router-dom";
import styled from "styled-components";
import { MODEL_NAMES } from "../../const/Const";
import { Title } from "../../global/StyledComps";
import {searchModelInstances} from "../../../api-calls/ApiCalls";
import ModelColumns from "./ModelColumns";

const Term = styled.q`
  color: gray;
  font-style: italic;
`;

const MainWrapper = styled.div`
  width: 85%;
  min-width: 900px;
  margin: auto;
`;

const Search = () => {
  const [searchParams] = useSearchParams({});
  const [data, setData] = useState({});
  const query = searchParams.get("q");

  useEffect(() => {
    let count = 0;
    const resultData = {};
    MODEL_NAMES.map((modelName) => {
      searchModelInstances(modelName, query).then((data) => {
        resultData[modelName] = data;
        // console.log(data);
        count++;
        if(count === 3){
          setData(resultData);
        }
      });
    });
	}, []);

  return (
    <MainWrapper>
      <Title>
        Results for&nbsp;
          <Term>
            {searchParams.get("q")}
          </Term>
      </Title>
      <hr />

      <ModelColumns
        data={data}
        query={query}
      />
    </MainWrapper>
  );
};

export default Search;
