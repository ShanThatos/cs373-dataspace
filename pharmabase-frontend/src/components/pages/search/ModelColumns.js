/* eslint-disable react/prop-types */
import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import ResultCard from "./ResultCard";
import { capFirstChar } from "../../../utils/Helpers";
import { Card } from "react-bootstrap";
import { MODEL_NAMES } from "../../const/Const";
import { CenteredSpinner } from "../../global/StyledComps";

const ModelColumnsWrapper = styled.div`
  display: flex;
  margin: auto;
`;

const NoResultText = styled.h3`
  color: grey;
  margin-top: 1rem;
`;

const ModelColumn = styled(Card)`
  min-height: 45rem;
  width: 33.33%;
  border-radius: 0px;
  margin-bottom: 1.5rem;
`;

const ColumnContext = styled.div`
  max-height: 750px;
  overflow-y: auto;
`;

const ModelColumns = (props) => {
  const {
    data, query
  } = props;

  return (
    <ModelColumnsWrapper>
      {MODEL_NAMES.map((modelName) => (
        <ModelColumn 
          key={modelName}
        >
          <Card.Header>
            <h3>
              {capFirstChar(modelName)}
            </h3>
          </Card.Header>
          <ColumnContext>
            {(data[modelName]) ?
              data[modelName].length > 0 ?
                data[modelName].map((item) => {
                  // let item = data[modelName][modelId];
                  return (
                    <ResultCard
                      key={item.preview_text.id}
                      id={item.preview_text.id}
                      name={item.name}
                      instanceData={item.preview_text}
                      wordsToHighlight={query.split(" ")}
                      modelName={modelName}
                    />
                  );
                })
              :
                <NoResultText>
                  No Results
                </NoResultText>
            :
              <CenteredSpinner animation="border" />
            }
          </ColumnContext>
        </ModelColumn>
      ))}
    </ModelColumnsWrapper>
  );
};

export default ModelColumns;