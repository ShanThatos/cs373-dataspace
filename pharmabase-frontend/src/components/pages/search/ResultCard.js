/* eslint-disable react/prop-types */
import React from "react";
import styled from "styled-components";
import { Card } from "react-bootstrap";
import PropTypes from "prop-types";
import Highlighter from "react-highlight-words";
import { Link } from "react-router-dom";
import { SEARCHABLES } from "../../const/Const";

const StyledCard = styled(Card)`
  border-style: none;
  text-align: left;
  white-space: pre-line;
`;

const ResultCard = (props) => {
  const {
    name, wordsToHighlight, instanceData,
    id, modelName
  } = props;

  // const attrs = SEARCHABLES[modelName];

  // temp till backend works
  // const stringInstanceData = JSON.stringify(instanceData); 

  return (
    <StyledCard >
      <Card.Body>
        <Card.Title>
          <Link
            to={`/${modelName}/${id}`}
          >
            <Highlighter
              searchWords={wordsToHighlight}
              textToHighlight={name}
            />
          </Link>
        </Card.Title>
        {Object.keys(instanceData).map((attr) => (
          <li
            key={attr}
            style={{ fontSize: "13px" }}
          >
            <b>{attr}: </b>
            <span>
              <Highlighter
                searchWords={wordsToHighlight}
                textToHighlight={`${instanceData[attr]}`}
              />
            </span>
          </li>
        ))}
      </Card.Body>
    </StyledCard>
  );
};

ResultCard.propTypes = {
  name: PropTypes.string.isRequired,
  // text: PropTypes.string.isRequired,
  wordsToHighlight: PropTypes.array,
  id: PropTypes.string.isRequired,
  modelName: PropTypes.string.isRequired
};

export default ResultCard;
