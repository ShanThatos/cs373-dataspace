import React from "react";
import { Col, Card } from "react-bootstrap";
import PropTypes from "prop-types";

const RepoStat = (props) => {
  return (
    <Col className="col-3 mx-3">
      <Card style={{ width: "15rem" }}>
        <Card.Body>
        <Card.Title>Number of {props.item}</Card.Title>
        <hr />
        <p>{props.count === -1 ? "...fetching data" : props.count}</p>
        </Card.Body>
      </Card>
    </Col>
  );
};

RepoStat.propTypes = {
  item: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
};

export default RepoStat;
