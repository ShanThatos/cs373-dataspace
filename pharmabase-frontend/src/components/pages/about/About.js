import React from "react";
import styled from "styled-components";
import { useState, useEffect } from "react";
import { gitLabProjectRoot, gitLabRepo } from "../../../components/const/Endpoints";
import { Container, Row } from "react-bootstrap";
import MemberCard from "./MemberCard.js";
import LinkCard from "./LinkCard.js";
import RepoStat from "./RepoStat.js";
// import "../../../styles/About.css";
import teamInfo from "./TeamInfo";
import {dataSources, toolsUsed} from "./SourceData.js";
import gitlab from "../../../images/resourceImages/gitlab.png";
import postman from "../../../images/resourceImages/postman.png";

const CardsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
`;

const CardContainer = styled.div`
  margin-bottom: 1.5rem;
`;

const About = () => {

  /* This code for getting the gitlab statistics was taken from texavotes.me */
  /* https://gitlab.com/forbesye/fitsbits */
	const [teamList, setTeamList] = useState([]);
	const [totalCommits, setTotalCommits] = useState(0);
	const [totalIssues, setTotalIssues] = useState(0);
	const [totalTests, setTotalTests] = useState(0);
	const [loaded, setLoaded] = useState(false);


	useEffect(() => {
		const fetchData = async () => {
			if (teamList === undefined || teamList.length === 0) {
				const gitlabInfo = await getGitlabInfo();
				setTotalCommits(gitlabInfo.totalCommits);
				setTotalIssues(gitlabInfo.totalIssues);
				setTotalTests(gitlabInfo.totalTests);
				setTeamList(gitlabInfo.teamInfo);
				setLoaded(true);
			}
		};
		fetchData();
	}, [teamList]);

  const getGitlabInfo = async () => {
    let totalCommitCount = 0,
      totalIssueCount = 0,
      totalTestCount = 0;

    // Need to wipe member issues before calling again and calculate total tests
    teamInfo.forEach((member) => {
      totalTestCount += (member.tests ? member.tests : 0);
      member.issues = 0;
      member.commits = 0;
    });

    // Can't use a map cause Gitlab's API returns are weird :/
    let commitList = await fetch(
      `${gitLabRepo}/contributors`
    );
    commitList = await commitList.json();
    commitList.forEach((element) => {
      const { name, email, commits } = element;
      teamInfo.forEach((member) => {
        if (
          member.name === name ||
          member.username === name ||
          member.email === email
        ) {
          member.commits += commits;
        }
      });
      totalCommitCount += commits;
    });

    // More than 100 issues, need to do pagination to get all
    const issuePaginationLength = 100;
    let page = 1;
    let issuePage = [];
    let issueList = [];
    do {
      issuePage = await fetch(
        `${gitLabProjectRoot}/issues?per_page=${issuePaginationLength}&page=${page++}`
      );
      issuePage = await issuePage.json();
      issueList = [...issueList, ...issuePage];
    } while (issuePage.length === 100);


    // we can decided what we want to issues number to be on the about pages
    // (is it the number of issues created?, or number of issues closed?, ....)
    issueList.forEach((element) => {
      const { assignees } = element;
      assignees.forEach((a) => {
        const { username } = a;
        teamInfo.forEach((member) => {
          if (member.name === username || member.username === username) {
            member.issues += 1;
          }
        });
      });
      totalIssueCount += 1;
    });

    return {
      totalCommits: totalCommitCount,
      totalIssues: totalIssueCount,
      totalTests: totalTestCount,
      teamInfo: teamInfo,
    };
  };
  /* end of citation */


  return (
    <Container className="jcontainer-fluid ustify-content-center">
      <h1 className="title">About Us</h1>

      <p className="mx-auto">
        PharmaBase is an intuitive tool that provides an easy avenue to learn more about the makeup of pharmaceutical drugs.
Many of us in the modern world take medicines and supplements to better our health, however we are often not as informed on the makeup of these drugs, and the interesting chemistry that goes into them. PharmaBase is a database of the most common and popular pharmaceutical drugs, that describes the drugs as well as the chemical compounds that makeup their active ingredients.
We also provide info on the chemical elements that make up the compounds themselves, to connect the complexities of pharmaceuticals to the basics of chemistry.</p>

      <CardsContainer>
        <LinkCard
          name= "GitLab Repository"
          picture={gitlab}
          link={"https://gitlab.com/ShanThatos/cs373-dataspace/-/tree/main"}
          descr="Pharmabase Repository"
        />
        <LinkCard
          name= "API Documentatioon"
          picture={postman}
          link={"https://documenter.getpostman.com/view/19991215/UVyyutJH"}
          descr="Postman API Documentation"
        />
    </CardsContainer>

  <h1 className="title">Group Members</h1>

      {loaded && (
        <CardsContainer>
          {teamList.map((member) => {
            return (
              <CardContainer key={member.username}>
                <MemberCard
                  name={member.name}
                  bio={member.bio}
                  commits={member.commits}
                  issues={member.issues}
                  profilePhoto={member.profilePhoto}
                  job={member.job}
                  tests={member.tests ? member.tests : 0}
                  leader={member.leader}
                />
              </CardContainer>
            );
          })}
        </CardsContainer>
      )}

      <h1 className="title">Repository Stats</h1>
      <Row className="justify-content-center">
        <RepoStat item="Commits" count={totalCommits}/>
        <RepoStat item="Issues" count={totalIssues}/>
        <RepoStat item="Unit Tests" count={isNaN(totalTests) ? 0 : totalTests}/>  {/* totalTests is NaN right now ow some reason */}
      </Row>

      <br/>
      <h1 className="title">Data Sources:</h1>

        <CardsContainer>
          {dataSources.map((source) => {
            return (
              <CardContainer key={source.name}>
                <LinkCard
                  name={source.name}
                  picture={source.picture}
                  link={source.link}
                  descr={source.descr}
                />
              </CardContainer>
            );
          })}
        </CardsContainer>

      <br /><br/>

      <h1 className="title">Tools Used:</h1>

        <CardsContainer>
          {toolsUsed.map((source) => {
            return (
              <CardContainer key={source.name}>
                <LinkCard
                  name={source.name}
                  picture={source.picture}
                  link={source.link}
                  descr={source.descr}
                />
              </CardContainer>
            );
          })}
        </CardsContainer>
    </Container>
  );
};

export default About;
