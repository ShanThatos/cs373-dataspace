import earlPhoto from "../../../images/memberPhotos/earlPhoto.jpg";
import peterPhoto from "../../../images/memberPhotos/peterPhoto.jpg";
//import profilePhoto from "../../../images/blank-profile.png";
import lucaPhoto from "../../../images/memberPhotos/lucaPic.jpg";
import samPhoto from "../../../images/memberPhotos/sam-pic.jpg";
import shanthPhoto from "../../../images/memberPhotos/shanthPic.jpg";

const teamInfo = [{
    name: "Luca Santos",
    email: "80182550+Luca-1999@users.noreply.github.com",
    commits: 0,
    issues: 0,
    photo: "",
    bio: "I am a senior international CS student from Brazil! Besides programming I am also an avid roguelike player (risk of rain 2 ftw) and hobby artist.",
    job: "Architecture, Backend, and Documentation",
    username: "luca.santos",
    profilePhoto: lucaPhoto,
    tests: 10,
    leader: "Phase II Leader"
  },
  {
    name: "Shanth Koka",
    email: "shanthatosgdev@gmail.com",
    commits: 0,
    issues: 0,
    photo: "shanthpic.jpeg",
    bio: "I'm a 2nd year student studying Computer Science at UT. Outside of programming I love to watch movies and play videogames. ",
    job: "Backend/Data Management and Frontend Development",
    username: "ShanThatos",
    profilePhoto: shanthPhoto,
    leader: "Phase I & Phase III Leader",
    tests: 86 + 14 // 86 postman unit tests and 14 backend unit tests
  },
  {
    name: "Peter Ma",
    email: "peterhma@utexas.edu",
    commits: 0,
    issues: 0,
    photo: "",
    bio: "I am a third year CS major at UT Austin and I come from Houston TX. I like to play Civilization V and kick some practice paddles in my free time.",
    job: "Frontend Development",
    username: "peterhma",
    profilePhoto: peterPhoto, 
    tests: 100 // 100 postman unit tests
  },
  {
    name: "Samson Broten",
    email: "brotensam@gmail.com",
    commits: 0,
    issues: 0,
    photo: "sam-pic.jpg",
    bio: "I am a junior CS student at UT austin. I am originally from Houston, Texas. I like to spend my free time chatting with my friends",
    job: "Frontend development and Deployment",
    username: "brotensam",
    profilePhoto: samPhoto,
    tests: 2
  },
  {
    name: "Earl Potts",
    email: "epotts0629@gmail.com",
    commits: 0,
    issues: 0,
    tests: 8,
    photo: "",
    bio: "I'm a fourth-year Computer Science and African & African Diaspora Studies double major. In my free time I like to hang out with my friends and play video games.",
    job: "Frontend Development",
    username: "earlpotts",
    profilePhoto: earlPhoto
  },
];



export default teamInfo;