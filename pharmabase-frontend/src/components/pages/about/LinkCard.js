import React from "react";
import { Card, Col } from "react-bootstrap";
import PropTypes from "prop-types";

const cardStyle = {
  height: "350px",
  width: "18rem",
	display: "flex",
	justifyContent: "center",
	alignItems: "center",
	textAlign: "center",
};

const imgStyle = {
  paddingTop: "10%",
	objectFit: "contain",
	width: "100%",
	height: "100%"
};

const descrStyle = {
  color: "gray"
};


const LinkCard = (props) => {
  return (
    <Col className="col-md-3 justify-content-center">
      <a href={props.link} style ={{ textDecoration: "none" }}>
        <Card className="m-3 justify-content-center" style={cardStyle} >
          <div style={{width: "250px", height: "200px"}}>
            <Card.Img variant="top" style = {imgStyle}src={props.picture} />
          </div>

          <hr/>
          <Card.Body>
            <Card.Title>{props.name}</Card.Title>{" "}

            <p style = {descrStyle}>{props.descr}</p>
          </Card.Body>
        </Card>
      </a>
    </Col>
  );
};

LinkCard.propTypes = {
  name: PropTypes.string.isRequired,
  picture: PropTypes.string,
  link: PropTypes.string,
  descr: PropTypes.string
};


export default LinkCard;
