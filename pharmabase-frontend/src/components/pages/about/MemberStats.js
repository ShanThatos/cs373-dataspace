import React from "react";
import { Col } from "react-bootstrap";
import PropTypes from "prop-types";

const MemberStats = (props) => {
  return (
    <Col className="col-4">
      <p className="memberStatText">{props.item}</p>
      <hr/>
      <p className="memberStatText">{props.count}</p>
    </Col>
  );
};

MemberStats.propTypes = {
  item: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired
};

export default MemberStats;
