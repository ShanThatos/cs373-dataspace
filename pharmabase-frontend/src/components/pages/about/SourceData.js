import periodicTable from "../../../images/resourceImages/periodicTable.png";
import chEMBL from "../../../images/resourceImages/chembl.png";
import mediaWiki from "../../../images/resourceImages/mediaWiki.svg";
import openFDA from "../../../images/resourceImages/openFDA.png";
import pubChem from "../../../images/resourceImages/pubChem.png";
import namecheap from "../../../images/resourceImages/namecheap.jpg";
import aws from "../../../images/resourceImages/aws.png";
import reactBootstrap from "../../../images/resourceImages/reactBootstrap.png";
import react from "../../../images/resourceImages/react.png";
import selenium from "../../../images/resourceImages/selenium.png";
import gitlab from "../../../images/resourceImages/gitlab.png";
import postman from "../../../images/resourceImages/postman.png";
import heroku from "../../../images/resourceImages/heroku.png";
import recharts from "../../../images/resourceImages/recharts.png";


export const dataSources = [
  {
    name: "openFDA API",
    link: "https://open.fda.gov/apis/drug/ndc/example-api-queries/",
    picture: openFDA,
    descr: "Used to collect drug data."
  },
  {
    name: "PubChem Docs",
    link: "https://pubchemdocs.ncbi.nlm.nih.gov/compounds",
    picture: pubChem,
    descr: "Used to collect data and images on chemical compoounds."
  },
  {
    name: "ChEMBL Interface Documentation",
    link: "https://chembl.gitbook.io/chembl-interface-documentation/web-services/chembl-data-web-services",
    picture: chEMBL,
    descr: "Used to collect data on chemical elements."
  },
  {
    name: "Periodic Table API",
    link: "https://github.com/neelpatel05/periodic-table-api",
    picture: periodicTable,
    descr: "Used to get info on the elements"
  },
  {
    name: "Media Wiki API",
    link: "https://www.mediawiki.org/wiki/API:Main_page",
    picture: mediaWiki,
    descr: "Used to collect images of our models"
  }
];

export const toolsUsed = [
  {
    name: "NameCheap",
    link: "https://www.namecheap.com/",
    picture: namecheap,
    descr: "Domain name hosting"
  },
  {
    name: "AWS",
    link: "https://aws.amazon.com/",
    picture: aws,
    descr: "Deployment service"
  },
  {
    name: "React",
    link: "https://reactjs.org/",
    picture: react,
    descr: "JavaScript library used too build app"
  },
  {
    name: "React Bootstrap",
    link: "https://react-bootstrap.github.io/",
    picture: reactBootstrap,
    descr: "Package for elements formatting"
  },
  {
    name: "GitLab",
    link: "https://www.gitlab.com",
    picture: gitlab,
    descr: "Version Control System"
  },
  {
    name: "Postman",
    link: "https://www.postman.com",
    picture: postman,
    descr: "API design and docs"
  },
  {
    name: "Selenium",
    link: "https://www.selenium.dev/",
    picture: selenium,
    descr: "GUI Testing suite"
  },
  {
    name: "Heroku",
    link: "https://www.heroku.com/",
    picture: heroku,
    descr: "Cloud backend service"
  },
  {
    name:"Recharts",
    link: "https://recharts.org/en-US",
    picture: recharts,
    descr: "React Visualization Library"    
  }

];
