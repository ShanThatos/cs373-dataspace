import React from "react";
import { Card, Row, Col } from "react-bootstrap";
import PropTypes from "prop-types";
import MemberStats from "./MemberStats";

const MemberCard = (props) => {
  return (
    <Col className="col-3">
      <Card style={{ width: "25rem" }}>
        <Card.Img variant="top" src={props.profilePhoto} />
        <Card.Body>
          <Card.Title>{props.name}</Card.Title>{" "}
          <Card.Text className="bioText">{props.job}</Card.Text>
          {props.leader && (<Card.Text className="bioText">{props.leader}</Card.Text>)}
          <Card.Text className="bioText">{props.bio}</Card.Text>
          <hr />
          <Row>
            <MemberStats item="Commits" count={props.commits} />
            <MemberStats item="Issues" count={props.issues} />
            <MemberStats item="Unit Tests" count={props.tests} />
          </Row>
        </Card.Body>
      </Card>
    </Col>
  );
};

MemberCard.propTypes = {
  name: PropTypes.string.isRequired,
  bio: PropTypes.string.isRequired,
  job: PropTypes.string.isRequired,
  photo: PropTypes.string.isRequired,
  commits: PropTypes.number.isRequired,
  issues: PropTypes.number.isRequired,
  tests: PropTypes.number.isRequired,
  profilePhoto: PropTypes.object.isRequired,
  leader: PropTypes.string
};

export default MemberCard;
