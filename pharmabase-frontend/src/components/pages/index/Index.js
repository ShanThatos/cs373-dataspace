import PropTypes from "prop-types";
import React, { useEffect, useRef, useState, } from "react";
import { ButtonGroup, Table, Button, InputGroup, FormControl, ToggleButton, Dropdown, Form, Row, Col, Tooltip, OverlayTrigger } from "react-bootstrap";
import { getFilteredInstances, getSchema } from "../../../api-calls/ApiCalls";
import { useLocation, useNavigate, } from "react-router-dom";
import { ALL_TABLE_ATTRIBUTES, convertAttributeNameToHeader, NORMALIZED_MODEL_HEADERS } from "../../const/Const";
import { Title } from "../../global/StyledComps";
import styled from "styled-components";
import { CenteredSpinner } from "../../global/StyledComps";
import Highlighter from "react-highlight-words";

const MainWrapper = styled.div`
  width: 90%;
  margin: auto;
`;

const Index = (props) => {
  const { modelName } = props;
  const normalizedName = NORMALIZED_MODEL_HEADERS[modelName];

  const { state } = useLocation();
  const defaultOptions = { skip: 0, limit: 10, sort: "name", sortorder: "ASC", filters: [] };
  const options = state && state.options ? state.options : defaultOptions;
  // window.history.replaceState(null, "");

  // const navigate = useNavigate();
  const [instanceCount, setInstanceCount] = useState(null);
  // const [options, setOptions2] = useState(storedOptions);
  const [data, setData] = useState(null);
  const pageInput = useRef(null);
  const [schema, setSchema] = useState(null);
  let dict = {};

  const navigate = useNavigate();
  const navigateToInstance = (link) => {
    console.log('clicked');
    navigate(link);
  };

  const setOptions = (x) => {
    // setOptions2(x);
    navigate(window.location.pathname + window.location.search, { state: { options: x } });
  };

  useEffect(() => {
    getSchema().then(setSchema);
  }, []);

  useEffect(() => {
    getFilteredInstances(modelName, options).then((res) => {
      setInstanceCount(res.total_count);
      setData(res.data);
    });
  }, [state]);

  const checkIfItemIsFiltered = (attr, item) => {
    for (const filter of options.filters)
      if (filter[0] === attr && filter[1] === "in" && filter[2].includes(item))
        return true;
    return false;
  };
  const getLikeFilteredItem = (attr) => {
    for (const filter of options.filters)
      if (filter[0] === attr && filter[1] === "like")
        return filter[2];
    return null;
  };


  if (instanceCount === null || data === null || schema === null)
    return <CenteredSpinner animation="border" />;

  const currentPage = options.skip / options.limit + 1;
  const setCurrentPage = (page) => {
    setOptions({
      ...options,
      skip: (page - 1) * options.limit,
    });
  };
  const totalPages = Math.ceil(instanceCount / options.limit);
  const navStartPage = Math.max(1, currentPage - 4);
  const navEndPage = Math.min(totalPages, navStartPage + 8);

  const modelAttributes = ALL_TABLE_ATTRIBUTES[modelName];

  const render_range = (input, test, attr) => {
    let newFilters = [];
    let num = input.target.value;

    let found_flag = false;
    options.filters.forEach(filter => {
      let temp = filter;
      if (filter[0] === attr && filter[1] === test) {
        console.log("found");
        temp[2] = num;
        found_flag = true;
        if (num) newFilters.push(temp);
      } else {
        newFilters.push(temp);
      }
    });

    if (!found_flag) {//add val to attribute filter
      console.log("not found");
      newFilters.push([attr, test, num]);
    }

    setOptions({ ...options, filters: newFilters, skip: 0 });
  };

  const render_header = (attr) => {
    const attr_dict = schema[modelName][attr];

    if (attr_dict["filter_type"] === "dropdown") {
      let highlight;
      return (
        <div className="mt-2" style={{ width: "100%", fontSize: "0.8rem" }}>
          <Dropdown>
            <Dropdown.Toggle className="btn-sm py-0" variant="outline-primary" id="dropdown-basic">
              Filter By:
            </Dropdown.Toggle>
            <Dropdown.Menu>
              {attr_dict["values"].map((val) => (
                <Dropdown.Item key={val}
                  onClick={() => {
                    highlight = false;
                    let newFilters = [];
                    let found_flag = false;
                    options.filters.forEach(filter => {
                      let temp = filter;
                      if (filter[0] === attr) {
                        if (!filter[2].includes(val)) {//filter found so check if val already present
                          temp[2].push(val); // if not push it into attribute val array
                          highlight = true;
                        } else {
                          // if not clear it from current array, aka toggle filter off
                          temp[2].splice(temp[2].indexOf(val), 1);

                        }
                        found_flag = true;
                      }
                      if (temp[2].length > 0) { newFilters.push(temp); }// if attribute filter array is not empty add it to newFilters
                    });

                    if (!found_flag) {//add val to attribute filter
                      newFilters.push([attr, "in", [val]]);
                    }

                    setOptions({ ...options, filters: newFilters, skip: 0 });
                  }} active={checkIfItemIsFiltered(attr, val)}>
                  {val}
                </Dropdown.Item>
              ))}
            </Dropdown.Menu>
          </Dropdown>
        </div>);
    } else if (attr_dict["filter_type"] === "range_number") {
      return (
        <Form className="mt-2">
          <Row>
            <Col>
              <Form.Control 
                className="form-control-sm" 
                type="number" placeholder="Start #"
                onChange={(e) => { render_range(e, "greaterthaneq", attr); }} 
              />
            </Col>
            <Col>
              <Form.Control 
                className="form-control-sm" 
                type="number" placeholder="End #"
                onChange={(e) => { render_range(e, "lessthaneq", attr); }} 
              />
            </Col>
          </Row>
        </Form>
      );
    } else if (attr_dict["filter_type"] === "range_date") {
      return (
        <div className="mt-2">
          <input type="date" onInput={(e) => { render_range(e, "greaterthaneq", attr); }} />
          <input type="date" onInput={(e) => { render_range(e, "lessthaneq", attr); }} />
        </div>
      );
    }
    return (
      <div>
        <input
          placeholder="Search"
          className="mt-2"
          type="text"
          style={{ width: "100%", fontSize: "0.8rem" }}
          value={getLikeFilteredItem(attr) || ""}
          onChange={(e) => {
            const search = e.target.value.trim();
            // previous filters: [["name","like","ad"]]
            // new filters: [["name","like","adapalene"]]

            let newFilters = options.filters.filter((f) => f[0] !== attr);
            if (search)
              newFilters.push([attr, "like", search]);
            setOptions({ ...options, filters: newFilters, skip: 0 });
          }}
        />
      </div>
    );
  };

  const render_headers = () => {
    return (
      <thead>
        <tr>
          <th>
            <div>#</div>
            <div>
              <Button
                variant="outline-primary"
                name="clearFiltersBtn"
                className="btn-sm mb-1"
                onClick={() => setOptions(defaultOptions)}
              >
                Clear
              </Button>
              <ToggleButton
                id="id-ASC"
                type="radio"
                variant="outline-primary"
                name="sortbtn"
                className="btn-sm px-1"
                checked={options.sort === "id" && options.sortorder === "ASC"}
                onChange={() => setOptions({ ...options, sort: "id", sortorder: "ASC" })}
              >
                <span className="text-nowrap">No sort</span>
              </ToggleButton>
            </div>
          </th>
          {modelAttributes.map((attr) => (
            <th key={attr}>
              <div>
                {convertAttributeNameToHeader(attr)}
              </div>
              <div>
                <ButtonGroup>
                  {[["↓", "ASC"], ["↑", "DESC"]].map(([label, sortorder]) => (
                    <ToggleButton
                      key={`${label}-${sortorder}-${attr}`}
                      id={`${attr}-${sortorder}`}
                      type="radio"
                      variant="outline-primary"
                      name="sortbtn"
                      className="btn-sm py-0"
                      checked={options.sort === attr && options.sortorder === sortorder}
                      onChange={() => setOptions({ ...options, sort: attr, sortorder })}
                    >
                      {label}
                    </ToggleButton>
                  ))}
                </ButtonGroup>
              </div>
              {render_header(attr)}
            </th>
          ))}
        </tr>
      </thead>
    );
  };

  const renderTooltip = (instance) => {
    if(modelName === "drugs" && instance.purpose){
      return (
        <Tooltip id="button-tooltip" {...instance}>
          {instance.purpose}
        </Tooltip>
      );
    }
    return (<div></div>);
  }; 

  const render_body = () => {
    return (
      <tbody style={{ fontSize: "13px" }}>
        {data.map((instance, index) => (
          <tr key={instance.id} onClick={() => navigateToInstance(`${location.pathname}/${instance.id}`)} style={{ cursor: "pointer" }}>
            <td>{options.skip + index + 1}</td>
            {modelAttributes.map((attr) => (
              <OverlayTrigger
              key={attr}
              placement="right"
              delay={{ show: 250, hide: 400 }}
              overlay={renderTooltip(instance)}
              >
              <td key={attr}>
                {getLikeFilteredItem(attr) !== null &&
                  <Highlighter
                    highlightClassName="bg-warning p-0"
                    searchWords={[getLikeFilteredItem(attr)]}
                    autoEscape={true}
                    textToHighlight={instance[attr]}
                    style={attr === "name" ? { fontWeight: "bold" } : {}}
                  />
                }
                {getLikeFilteredItem(attr) === null &&
                  <span style={attr === "name" ? { fontWeight: "bold" } : {}}>{instance[attr]}</span>
                }
              </td>
              </OverlayTrigger>
            ))}
          </tr>
        ))}
      </tbody>
    );
  };

  const get_pagination = () => {
    return (
      <ButtonGroup className="mx-2">
        <Button
          className="text-center px-1 border-1 border-dark"
          onClick={() => setCurrentPage(1)}
          style={{ width: "50px" }}
          variant={1 === currentPage ? "secondary" : "light"}
        >
          <span className={1 !== currentPage ? "text-primary" : ""}>
            First
          </span>
        </Button>
        {currentPage > 1 &&
          <Button
            className="text-center px-1 border-1 border-dark"
            onClick={() => setCurrentPage(currentPage - 1)}
            style={{ width: "50px" }}
            variant="light"
          >
            <span className="text-primary">
              Prev
            </span>
          </Button>
        }
        {[...Array(navEndPage - navStartPage + 1)].map((x, i) => {
          let page = navStartPage + i;
          return (
            <Button
              className="text-center px-1 border-1 border-dark"
              key={page}
              onClick={() => setCurrentPage(page)}
              style={{ width: "40px" }}
              variant={page === currentPage ? "secondary" : "light"}
            >
              <span className={page !== currentPage ? "text-primary" : ""}>
                {page}
              </span>
            </Button>
          );
        })}
        {currentPage < totalPages &&
          <Button
            className="text-center px-1 border-1 border-dark"
            onClick={() => setCurrentPage(currentPage + 1)}
            style={{ width: "50px" }}
            variant="light"
          >
            <span className="text-primary">
              Next
            </span>
          </Button>
        }
        <Button
          className="text-center px-1 border-1 border-dark"
          onClick={() => setCurrentPage(totalPages)}
          style={{ width: "50px" }}
          variant={totalPages === currentPage ? "secondary" : "light"}
        >
          <span className={totalPages !== currentPage ? "text-primary" : ""}>
            Last
          </span>
        </Button>
      </ButtonGroup>
    );
  };

  const render_goTo = () => {
    return (
      <>
        <div className="mt-2">
          <Form.Select size="sm"
            className="mr-2"
            onChange={(e) => {
              const numResultsPerPage = parseInt(e.currentTarget.value);
              setOptions({ ...options, skip: 0, limit: numResultsPerPage });
            }}
            defaultValue={options.limit}
          >
            {[5, 10, 25, 50, 100].map(num => 
              <option key={num} value={num}>{num} Per Page</option>
            )}
          </Form.Select>
        </div>
        <InputGroup className="mx-2" style={{ maxWidth: "150px" }} >
          <FormControl ref={pageInput} className="form-control-sm" type="number" placeholder="Page #" />
          <Button
            variant="secondary"
            onClick={() => {
              const page = parseInt(pageInput.current.value);
              if (page > 0 && page <= totalPages)
                setCurrentPage(page);
            }}
          >
            Go
          </Button>
        </InputGroup>
        <span className="me-2 px-1 py-2 border border-3 border-primary rounded">Page: {currentPage}/{totalPages}</span>
        <span className="px-1 py-2 border border-3 border-primary rounded">{instanceCount} Total Instances</span>
      </>
    );
  };

  return (
    <MainWrapper>
      <Title>
        {normalizedName} Index
      </Title>
      <hr />

      <Table className="mx-auto" striped bordered hover>
        {render_headers()}
        {render_body()}
      </Table>

      <div className="mb-5 d-flex flex-row justify-content-center">
        {get_pagination()}
        {render_goTo()}
      </div>
    </MainWrapper>
  );
};
export default Index;

Index.propTypes = {
  modelName: PropTypes.string
};
