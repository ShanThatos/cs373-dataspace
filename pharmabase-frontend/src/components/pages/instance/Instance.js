import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { PropTypes } from "prop-types";
import { useParams } from "react-router-dom";
import { findMedia, getSingleInstanceByID, getSingleInstanceConnections, getSingleInstanceMedia } from "../../../api-calls/ApiCalls";
import MediaCarousel from "./media-carousel/MediaCarousel";
import { MODEL_NAMES } from "../../const/Const";
import { CenteredSpinner, Title } from "../../global/StyledComps";
import InstanceText from "./InstanceText";
import InstanceTable from "./InstanceTable";
import Connections from "./connections/Connections";

const MainWrapper = styled.div`
  width: 65%;
  min-width: 800px;
  margin: auto;
`;

const TitleHr = styled.hr`
  border-top: 3px solid;
`;

const Instance = (props) => {
  const { modelName } = props;
  const { id } = useParams();

  const [instanceData, setInstanceData] = useState(null);
  const [instanceMedia, setInstanceMedia] = useState(null);
  const [connections, setConnections] = useState(null);

  const getConnections = async () => {
    let otherModels = [...MODEL_NAMES];
    otherModels.splice(otherModels.indexOf(modelName), 1);

    let newConnections = {};
    for (const om of otherModels)
      newConnections[om] = await getSingleInstanceConnections(modelName, id, om);

    console.log("conns", newConnections);
    setConnections(newConnections);
  };

  useEffect(() => {
    getSingleInstanceByID(modelName, id)
      .then(setInstanceData);
    getSingleInstanceMedia(modelName, id)
      .then(setInstanceMedia);
    getConnections();
  }, []);

  const wikiTextMedia = findMedia(instanceMedia, "wikimain", "raw_text");

  return (
    <MainWrapper>
      {instanceData === null || connections === null ?
        <CenteredSpinner animation="border" />
      :
        <>
          <Title>
            {instanceData.name}
          </Title>
          <TitleHr />

          <MediaCarousel
            instanceMedia={instanceMedia}
            instanceData={instanceData}
            modelName={modelName}
            connections={connections}
          />
          <hr />

          <InstanceText
            wikiTextMedia={wikiTextMedia}
            instanceData={instanceData}
            modelName={modelName}
          />
          <hr />

          <InstanceTable
            instanceData={instanceData}
          />
          <hr />
          
          <Connections 
            connsObj={connections}
          />
        </>
      }
    </MainWrapper>
  );
};

export default Instance;

Instance.propTypes = {
  modelName: PropTypes.string
};
