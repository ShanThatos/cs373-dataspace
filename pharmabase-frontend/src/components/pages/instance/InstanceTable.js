/* eslint-disable react/prop-types */
import React from "react";
import AttributeTable from "../../global/tables/AttributeTable";

const InstanceTable = (props) => {
  const { instanceData } = props;

  return (
    <>
      {/* TODO: change the way the data is shown for instances */}
      <div style={{ fontSize: "14px" }}>
        <AttributeTable instanceData={instanceData} />
      </div>
    </>
  );
};

export default InstanceTable;