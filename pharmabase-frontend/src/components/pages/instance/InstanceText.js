/* eslint-disable react/prop-types */
import React from "react";
import styled from "styled-components";

const TextWrapper = styled.div`
  margin-top:  1rem;
  margin-bottom:  1rem;
`;

const ModelText = (props) => {
  const {
    wikiTextMedia,
    instanceData,
    modelName,
  } = props;



    if (wikiTextMedia === null || wikiTextMedia === ""){
      var generatedTextMedia = ""
      const model = modelName.substring(0, modelName.length - 1)
      if (modelName === "drugs") {
        generatedTextMedia = `${instanceData.name} is a ${(instanceData.marketing_status).toLowerCase()}
          ${model}, manufactured by ${instanceData.manufacturer_name}. It is classified as a 
          ${(instanceData.product_type).toLowerCase()}.`
      }
      else if (modelName === "compounds") {
        let hdc = instanceData.hydrogen_donor_count;
        let hac = instanceData.hydrogen_acceptor_count;
        if (hdc === undefined) {
          hdc = 0
        } 
        if (hac === undefined) {
          hac = 0
        }
        generatedTextMedia = `${instanceData.name} has molecular formula: ${(instanceData.molecular_formula)}. 
        It has a heavy (non-hydrogen) atom count of ${instanceData.heavy_atom_count}. ${instanceData.name} has a
        hydrogen donor count of ${hdc} and a hydrogen acceptor count of ${hac}.`
      }
      else if (modelName === "elements") {
        generatedTextMedia = `${instanceData.name} (${instanceData.symbol}), disovered in ${instanceData.year_discovered} 
        is a ${instanceData.group_block} and has an atomic mass of ${(instanceData.atomic_mass)}. 
        It has a heavy (non-hydrogen) atom count of ${instanceData.heavy_atom_count}.`
      } 

      return (
      <TextWrapper>
        <p className="mx-3" style={{ fontSize: "15px", textAlign: "justify", lineHeight: 1.4 }}>
          {generatedTextMedia}
        </p>
      </TextWrapper>
      )
  }
  
   

  return (
    <TextWrapper>
      <p className="mx-3" style={{ fontSize: "15px", textAlign: "justify", lineHeight: 1.4 }}>
        {wikiTextMedia !== null && wikiTextMedia}
      </p>
    </TextWrapper>
  );
};

export default ModelText;