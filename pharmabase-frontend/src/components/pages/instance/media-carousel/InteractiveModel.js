/* eslint-disable react/prop-types */
import React from "react";
import styled from "styled-components";
import { useGLTF, OrbitControls } from "@react-three/drei";
import { Canvas } from "@react-three/fiber";

const ModelWrapper = styled.div`
  height: 300px;
  margin-top: 1rem;
`;

const InteractiveModel = (props) => {
  const { mediaData } = props;
  const { scene } = useGLTF(mediaData);

  return (
    <>
      <ModelWrapper>
        <Canvas camera={{ position: [30, 20, 20], fov: 0.85 }}>
        <pointLight position={[10, 10, 10]} intensity={1} />
          <primitive object={scene} />
          <OrbitControls />
        </Canvas>

      </ModelWrapper>
      <i>
        (Drag model to inspect)
      </i>
    </>
  );
};

export default InteractiveModel;