import { PropTypes } from "prop-types";
import React from "react";
import styled from "styled-components"; 
import InteractiveModel from "./InteractiveModel";

const StyledImg = styled.img`
  max-height: 300px;
  max-width: 600px;
  margin-top: 1.5rem;
`;

const ModelMedia = (props) => {
  const media = props.media;

  return (
    <>
      {media.media_type === "image_url" && 
        <StyledImg
          src={media.media_data}
        />
      }
      {media.media_type === "glb_url" && 
        <InteractiveModel 
          mediaData={media.media_data}
        />
      }
    </>
  );
};

export default ModelMedia;

ModelMedia.propTypes = {
  media: PropTypes.object
};