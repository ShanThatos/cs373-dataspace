/* eslint-disable react/prop-types */
import React from "react";
import styled from "styled-components";
import { Carousel } from "react-bootstrap";
import ModelMedia from "./InstanceMedia";
import { COMPOUNDS_URL_PARAM, DRUGS_URL_PARAM } from "../../../const/Const";
import ElementPieChart from "./ElementPieChart";

const StyledCarousel = styled(Carousel)`
  background-color: #C0C0C0;
  padding-top: 1rem;
  height: 400px;
`;

const MediaCarousel = (props) => {
  const { 
    instanceMedia, instanceData, modelName,
    connections
  } = props;

  const getDrugFormula = () => {
    let formula = "";
    try {
      for (const compound of connections.compounds)
          formula += compound.molecular_formula;
    } catch (err) {
      console.log("Uh oh");
    }
    return formula;
  };

  return (
    <StyledCarousel 
      variant="dark"
    >
      {/* PICS */}
      {instanceMedia
        .filter((x) => ["image_url", "glb_url"].includes(x.media_type))
        .map((x) => (
          <Carousel.Item 
            key={x.id}
          >
            <ModelMedia 
              media={x} 
            />
          </Carousel.Item>
        ))
      }

      {/* PIE CHART */}
      {modelName === COMPOUNDS_URL_PARAM && 
        <Carousel.Item>
          <ElementPieChart 
            formula={instanceData.molecular_formula}
          />
        </Carousel.Item>
      }
      {modelName === DRUGS_URL_PARAM && 
        <Carousel.Item>
          <ElementPieChart
            formula={getDrugFormula()}
          />
        </Carousel.Item>
      }
    </StyledCarousel>
  );
};

export default MediaCarousel;