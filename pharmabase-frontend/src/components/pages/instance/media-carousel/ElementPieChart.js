import React from "react";
// import { PieChart } from "react-minimal-pie-chart";
import styled from "styled-components";
import { PropTypes } from "prop-types";
import { getPieChartData } from "../../../../utils/Helpers";
import { Pie, PieChart, Tooltip } from "recharts";

const PieChartContainer = styled.div`
  /* height: 16rem; */
  margin: auto;
  margin-top: 0rem;
  
`;

const StyledPieChart = styled(PieChart)`
  margin: auto;
`;

const PieChartTitle = styled.h4`
  margin-top: -1.5rem;
`;

const ElementPieChart = (props) => {
  const { formula } = props;

  // console.log("FORMULA", formula);
  // formula = "C6H12Fe5C10"
  
  return (
    <div>
      <PieChartContainer>
        <StyledPieChart 
          width={320} 
          height={320}
        >
          <Pie
            dataKey="value"
            isAnimationActive={false}
            data={getPieChartData(formula)}
            cx="50%"
            cy="50%"
            fill="#0072bd"
            label={(entry) => entry.name}
          />
          <Tooltip />
        </StyledPieChart>
        <PieChartTitle>
          <i>Elemental Makeup by Mass (g)</i>
        </PieChartTitle>
      </PieChartContainer>
    </div>
  );
};


ElementPieChart.propTypes = {
  formula: PropTypes.string

};

export default ElementPieChart;