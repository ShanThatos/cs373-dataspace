import React from "react";
import styled from "styled-components";
import { Card } from "react-bootstrap";
import { capFirstChar } from "../../../../utils/Helpers";
import PropTypes from "prop-types";


const StyledCard = styled(Card)`
  width: 48%;
  max-height: 22rem;
`;

const StyledCardBody = styled(Card.Body)`
  overflow-y: auto;
  text-align: left;
`;

const ConnsCard = (props) => {
  const {
    model,
    conns,
  } = props;

  return (
      <StyledCard>
        <Card.Header>
          {capFirstChar(model)} Connections
        </Card.Header>
        <StyledCardBody>
          {conns.map((conn) => (
            <div key={conn.id}>
              <a 
                href={`/${model}/${conn.id}`}
              >
                {conn.name}
              </a>
            </div>
          ))}
        </StyledCardBody>
      </StyledCard>
  );
};

export default ConnsCard;

ConnsCard.propTypes = {
  model: PropTypes.string.isRequired,
  conns: PropTypes.arrayOf(PropTypes.object),
};
