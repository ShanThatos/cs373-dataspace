import React, {useState, useEffect} from "react";
import styled from "styled-components";
import { getSingleInstanceConnections } from "../../../../api-calls/ApiCalls";
import { MODEL_TYPES } from "../../../const/Const";
import { MODEL_NAMES } from "../../../const/Const";
import PropTypes from "prop-types";
import ConnsCard from "./ConnsCard";

const ConnectionsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  margin-top: 2rem;
  margin-bottom: 2rem;
`;

const Connections = (props) => {
  const { 
    connsObj
  } = props;

  return (
    <ConnectionsContainer>
      {Object.keys(connsObj).map((modelName) => (
        <ConnsCard
          key={modelName} 
          model={modelName}
          conns={connsObj[modelName]}
        />
      ))}
    </ConnectionsContainer>
  );
};

export default Connections;

Connections.propTypes = {
  connsObj: PropTypes.object.isRequired
};

