import styled from "styled-components";
import { Spinner } from "react-bootstrap";

export const Title = styled.h1`
  text-align: left;
  margin-top: 0.5rem;
  margin-bottom: -0.5rem;
  margin-left: 0.5rem;
`;

export const CenteredSpinner = styled(Spinner)`
  display: block;
  margin: auto;
  margin-top: 2rem;
  margin-bottom: 2rem;
`;