import PropTypes from "prop-types";
import React from "react";
import { Spinner, Table } from "react-bootstrap";
import styled from "styled-components";
import { NAME_ATTR } from "../../const/Const";
import { Link, useLocation, useNavigate } from "react-router-dom";
import Pagination from "./Pagination";

const TableContainer = styled.div`
  margin: auto;
  margin-bottom: 1rem;
  margin-top: 1rem;
  width: 80%;
  overflow-x: auto;
`;

const ModelTable = (props) => {
  const {
    tableHeaders,
    attributes,
    instances,
    size_per_page,
    max_size,
    set_func,
    URL
  } = props;

  const location = useLocation();

  /*
    HOW MODEL INDEX TABLE WORKS:
    - In Const.js, in 2 arrays, we specifiy what the
      table headers are, and the attribute of the object
      that correspond to the table header name.

      - manufacturer_name <--> Manufacturer Name
      (Table Column name is Manufacturer Name)
      (and we call instance[manufacturer_name] to show data)

    - This way we can just loop through all that stuff
  */

  const navigate = useNavigate();
  //method needed to make the entire rows clickable
  const navigateToInstance = (link) => {
    navigate(link);
  }

  const renderTableHeaders = () => {
    return tableHeaders.map((header) => (
      <th key={header}>
        {header}
      </th>
    ));
  };

  const renderTableRows = () => {
    return instances.map((instance) => (
      <tr key={instance.id} onClick={() => navigateToInstance(`${location.pathname}/${instance.id}`)}>
        {attributes.map((attr, index) => (
          // if the attribute is the name attribute, make it
          // a link to the instance page
          (attr === NAME_ATTR) ?
            <td key={index}>
              <Link style={{textDecoration: "none", color:"#212529"}} to={`${location.pathname}/${instance.id}`}>
                <b>{instance[attr]}</b>
              </Link>
            </td>
          :
            <td key={index}>
              {instance[attr]}
            </td>
        ))}
      </tr>
    ));
  };

  return (
    <div>
      {/* if the data has not been fetched yet, show
        a loading spinner */}

      {instances.length === 0 &&
        <Spinner animation="border" />
      }
      {instances.length !== 0 &&
        <div>
          <TableContainer>
            <Table striped bordered hover>
              <thead>
                <tr>
                  {renderTableHeaders()}
                </tr>
              </thead>
              <tbody>
                {renderTableRows()}
              </tbody>
            </Table>
          </TableContainer>

          <Pagination
            size_per_page={size_per_page}
            max_size={max_size}
            set_func={set_func}
            URL={URL}
          />
        </div>
      }

    </div>
  );
};

export default ModelTable;

ModelTable.propTypes = {
  tableHeaders: PropTypes.array,
  attributes: PropTypes.array,
  instances: PropTypes.array,
  size_per_page: PropTypes.number,
  max_size: PropTypes.number,
  set_func: PropTypes.any,
  URL: PropTypes.string
};
