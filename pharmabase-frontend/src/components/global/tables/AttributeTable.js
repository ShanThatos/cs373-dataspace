import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { Table, Spinner } from "react-bootstrap";
import _ from "lodash";

const TableContainer = styled.div`
  margin: auto;
  margin-bottom: 1rem;
  margin-top: 1rem;
  width: 80%;
  overflow-x: auto;
`;

const AttributeTable = (props) => {
  const {
    instanceData
  } = props;

  const attributes = Object.keys(instanceData);
  console.log(instanceData);

  // const getRowData = (attr) => {
  //   const rowData = instanceData[attr];
  //   if (rowData) {// if number return as is, else return string w first letter uppercase
  //     return (typeof rowData === 'number'? rowData: _.startCase(_.lowerCase(rowData)));
  //   } else {
  //     return 0;
  //   }
  // };

  const renderTableRows = () => {
    return attributes.map((attr) => (
      <tr key={attr}>
        <td>{_.startCase(attr)}</td>
        <td>{instanceData[attr]? instanceData[attr]: 0}</td>
      </tr>
    ));
  };

  return (
    <div>
      {/* if the data has not been fetched yet, show
        a loading spinner */}

      {instanceData.size === 0 &&
        <Spinner animation="border" />
      }
      {instanceData.size !== 0 &&
        <TableContainer>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th id='attribute'>Attribute</th>
                <th id='value'>Values</th>
              </tr>
            </thead>
            <tbody>
              {renderTableRows()}
            </tbody>
          </Table>
        </TableContainer>
      }
    </div>
  );
};

AttributeTable.propTypes = {
  instanceData: PropTypes.object,
};

export default AttributeTable;
