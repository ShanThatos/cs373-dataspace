import React, { useState} from "react";
import PropTypes from "prop-types";
import { getInstances } from "../../../archived/ApiCalls";

//reference: https://www.youtube.com/watch?v=qDTAWSg41ag

const Pagination = (props) => {

    const {
        size_per_page,
        max_size,
        set_func,
        URL} = props;
    
    // max_idx is the maximum 0-based pagination index we will traverse
    const max_idx = Math.floor(max_size/size_per_page)-1 + (max_size%size_per_page != 0? 1: 0);
    const [curPage, setPage] = useState(0);
    const page_nums = [];

    for (let i = 0; i <= max_idx; i++) {
        page_nums.push(i+1);
    }
    

    //Error, always fetching by ten, guard get instances by wether we need to issue api call
    const nextPage = () => {
        // pressing next on the last available page must not send a fetch request for data that does not exist
        const page_idx = curPage == max_idx? max_idx: curPage + 1;
        setPage(page_idx);
        getInstances(set_func, URL, (page_idx)*size_per_page, size_per_page);
    };
      
    const prevPage = () => {
        // avoid negative search indices
        const page_idx = curPage == 0? 0: curPage - 1;
        setPage(page_idx);
        getInstances(set_func, URL, (page_idx)*size_per_page, size_per_page);
    };

    const fixPage = num => {
        // accessing a pagination index that does not correspond to a group of data should be impossible
        const page_idx = (num-1) > max_idx? curPage: (num-1);
        console.log(page_idx);
        setPage(page_idx);
        getInstances(set_func, URL, (page_idx)*size_per_page, size_per_page);
    };

    const render_prefix = () => {
        
        if (curPage > 9) {//parameterize this to max page range
            return(
                <ul className="pagination justify-content-center">
                    <li className="page-item">
                        <a href="#" onClick={() => fixPage(1)} className="page-link">First</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link">&#8230;</a>
                    </li>
                </ul>
            );
        }
    };

    const render_page_nums = () => {
        const start_idx = Math.floor(curPage/10)*10;
        const render_slice = page_nums.slice(start_idx, (start_idx+10));
        return(
            <ul className="pagination justify-content-center">
                {render_slice.map(num => (
                    <li className="page-item" key={num}>
                        <a href="#" onClick={() => fixPage(num)} className="page-link">{num}</a>
                    </li>                    
                ))}
            </ul>
        );
    };

    const render_suffix = () => {

        const end_idx =(Math.floor(curPage/10)*10)+10;
        console.log(max_idx);
        if (end_idx <= (max_idx)) {
            return (
                <ul className="pagination justify-content-center">
                    <li className="page-item">
                        <a className="page-link">&#8230;</a>
                    </li>
                    <li className="page-item">
                        <a href="#" onClick={() => fixPage(page_nums.slice(-1))} className="page-link">Last</a>
                    </li>
                </ul>
            );
        }
    };

    return (
        <nav>
            <ul className="pagination justify-content-center">
                <li className="page-item">
                    <a className="page-link" href="#" onClick={() => prevPage()}>prev</a>
                </li>
                {render_prefix()}
                {render_page_nums()}
                {render_suffix()}
                <li className="page-item">
                    <a className="page-link" href="#" onClick={() => nextPage()}>next</a>
                </li>
            </ul>
        </nav>
    );
};

export default Pagination;


Pagination.propTypes = {
    size_per_page: PropTypes.number,
    max_size: PropTypes.number,
    set_func: PropTypes.any,
    URL: PropTypes.string
  };
