export const getWikiContentEP = (title) => {
  return `https://en.wikipedia.org/w/api.php?action=query&titles=${title}&prop=extracts&format=json`;
};

export const gitLabRepo = "https://gitlab.com/api/v4/projects/33894835/repository";
export const gitLabProjectRoot = "https://gitlab.com/api/v4/projects/33894835";

export const apiEndpointRoot = "https://pharmabase-backend.herokuapp.com/api";
