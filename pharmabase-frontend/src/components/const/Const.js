/* eslint-disable quotes */

export const MODEL_TYPES = {
  DRUG: 0,
  COMPOUND: 1,
  ELEMENT: 2,
};
// use MODEL_NAMES[MODEL_TYPES.DRUG] for drugs, etc.
export const MODEL_NAMES = [
  "drugs",
  "compounds",
  "elements",
];

export const NORMALIZED_MODEL_HEADERS = {
  "drugs": "Drugs",
  "compounds": "Compounds",
  "elements": "Elements"
};

export const apiHeader = { // I dont think we need this anymore
  "Accept": "application/vnd.api+json"
};

// could probably take these out in favor of the MODEL_NAMES array 
export const DRUGS_URL_PARAM = "drugs";
export const ELEMENTS_URL_PARAM = "elements";
export const COMPOUNDS_URL_PARAM = "compounds";

export const NAME_ATTR = "name";


// searchable attributes of each model
const elementSearchables = [
  "name",
  "symbol",
  "atomic_num",
  "standard_state",
  "group_block",
];
const compoundSearchables = [
  "name",
  "iupac_name",
  "iupac_inchi",
  "iupac_inchi_key",
  "molecular_formula",
];
const drugSearchables = [
  "name",
  "spl_id",
  "brand_name",
  "manufacturer_name",
  "dosage_form",
];
export const SEARCHABLES = {
  "drugs": drugSearchables,
  "compounds": compoundSearchables,
  "elements": elementSearchables,
};



// drug table
export const drugAttributes = [
  "name",
  "brand_name",
  "dosage_form",
  "manufacturer_name",
  "marketing_status",
  "product_type",
  "route_of_administration",
  "first_submission_date",
  "last_submission_date",
];
export const drugTableHeaders = [
  "Name",
  "Brand Name",
  "Dosage Form",
  "Manufacturer Name",
  "Marketing Status",
  "Product Type",
  "Route of Administration",
  "First Submission Date",
  "Last Submission Date",
];

// elements table
export const elementAttributes = [
  "name",
  "symbol",
  "atomic_mass",
  "atomic_num",
  "atomic_radius",
  "boiling_point",
  "group_block",
  "ionization_en",
  "standard_state",
  "year_discovered",
];
export const elementTableHeaders = [
  "Name",
  "Symbol",
  "Atomic Mass",
  "Atomic Number",
  "Atomic Radius",
  "Boiling Point",
  "Group Block",
  "Ionization Energy",
  "Standard State",
  "Year Discovered",
];

// compounds table
export const compoundAttributes = [
  "name",
  "molecular_formula",
  "molecular_weight",
  
  /* These values seem to always be null */
  "heavy_atom_count",
  "hydrogen_bond_acceptor_count",
  "hydrogen_bond_donor_count",
  "rotatable_bond_count",

  /* Probably need some function to run this inchi stuff through */
  // `iupac_inchi: "InChI=1S/C10H16N2O8.Ca.2Na/c13-7(14)3-11(4-8(15)16)1-2-12(5-9(17)18)6-10(19)20;;;/h1-6H2,(H,13,14)(H,15,16)(H,17,18)(H,19,20);;;/q;+2;2*+1/p-4"`,
  // `iupac_inchi_key: "SHWNNYZBHZIQQV-UHFFFAOYSA-J"`,
  // `iupac_name: "calcium;disodium;2-[2-[bis(carboxylatomethyl)amino]ethyl-(carboxylatomethyl)amino]acetate"`,
];
export const compoundTableHeaders = [
  "Name",
  "Molecular Formula",
  "Molecular Weight",
  "Heavy Atom Count",
  "Hydrogen Bond Acceptor Count",
  "Hydrogen Bond Donor Count",
  "Rotatable Bond Count",
];


export const ALL_TABLE_ATTRIBUTES = {
  "drugs": drugAttributes,
  "compounds": compoundAttributes,
  "elements": elementAttributes
};

// github copilot did some of this lol
const headerOverrides = {
  "atomic_num": "Atomic Number",
  "ionization_en": "Ionization Energy"
};
export const convertAttributeNameToHeader = (attributeName) => {
  if (headerOverrides[attributeName])
    return headerOverrides[attributeName];
  let header = attributeName.replace(/_/g, " ");
  header = header.replace(/\w\S*/g, (txt) => (txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()));
  return header;
};