import React from "react";
import {
  BrowserRouter, Route, Routes
} from "react-router-dom";
import NavigationBar from "./components/nav/NavigationBar";
import About from "./components/pages/about/About";
import Search from "./components/pages/search/Search";
import Index from "./components/pages/index/Index";
import Instance from "./components/pages/instance/Instance";
import Splash from "./components/pages/splash/Splash";
import { MODEL_NAMES } from "./components/const/Const";
import { UniVis } from "./components/pages/visualizations/univercity/UniVis";
import { PharmaVis } from "./components/pages/visualizations/pharmabase/PharmaVis";

const App = () => {

  return (
    // based on the url (path), we will render a certain
    // component (the thing in the element param)

    <div style={{ textAlign: "center" }}>

      <BrowserRouter>
        <NavigationBar />
        <Routes>
          <Route path="/" element={<Splash />} />
          <Route path="/about" element={<About />} />
          <Route path="/search" element={<Search />} />

          {MODEL_NAMES.map((model) => (
            <Route 
              key={`${model}-index`} 
              path={`/${model}`} 
              element={<Index modelName={model} />} 
            />
          ))} 

          {MODEL_NAMES.map((model) => (
            <Route 
              key={`${model}-instance`} 
              path={`/${model}/:id`} 
              element={<Instance modelName={model} />}
            />
          ))}

          <Route path="/vis/pharma" element={<PharmaVis />} />
          <Route path="/vis/uni" element={<UniVis />} />

        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default App;
