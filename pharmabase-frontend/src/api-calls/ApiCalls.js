import _ from 'lodash';

const API_ENDPOINT_ROOT = "https://api.pharmabase.me";
// const API_ENDPOINT_ROOT = "http://localhost:5000/";

const MODEL_ENDPOINT = API_ENDPOINT_ROOT + "/{modelName}";
const MODEL_CONNECTIONS_ENDPOINT = API_ENDPOINT_ROOT + "/connections/{modelName}/{id}/{connectionName}";
const SCHEMA_ENDPOINT = API_ENDPOINT_ROOT + "/schema";
const ALLOWED_FILTER_OPERATORS = ["eq", "noteq", "like", "lessthan", "greaterthan", "lessthaneq", "greaterthaneq", "in"];
const MODEL_CONNECTION_TABLES = {
    "drugs-compounds": "drugcompound",
    "compounds-drugs": "drugcompound",
    "compounds-elements": "compoundelement",
    "elements-compounds": "compoundelement",
    "drugs-elements": "drugelement",
    "elements-drugs": "drugelement"
};
const ALLOWED_MEDIA_SRCS = ["wikimain", "wikilame", "pubchem", "google-elements"];
const ALLOWED_MEDIA_TYPES = ["raw_text", "raw_html", "image_url", "glb_url"];

export const getSchema = async () => {
    let schema = localStorage.getItem("schema-cached-value");
    if (schema !== null) {
        try {
            schema = JSON.parse(schema);
            return schema;
        } catch (e) { console.log(e); }
    }
    schema = await fetchJSON(SCHEMA_ENDPOINT);
    try {
        localStorage.setItem("schema-cached-value", JSON.stringify(schema));
    } catch (e) { console.log(e); }
    return schema;
};

export const getInstanceCount = async (modelName) => {
    const schema = await getSchema();
    if (!objectContains(schema, modelName))
        throw new Error(`Could not find model: ${modelName}`);

    let url = makeURL(MODEL_ENDPOINT, {"modelName": modelName, "count": "true"});
    console.log("URL: ", url);
    return (await fetchJSON(url)).total_count;
};

export const getAllInstances = async (modelName) => {
    alert("Avoid getAllInstances -- it's slow");
    return await getFilteredInstances(modelName);
};

export const getSingleInstanceByID = async (modelName, id) => {
    if (!objectContains(await getSchema(), modelName))
        throw new Error(`Could not find model: ${modelName}`);
    let url = makeURL(MODEL_ENDPOINT, {"modelName": modelName, "id": id});
    console.log(url);
    return (await fetchJSON(url)).data[0];
};

// export const getSingleInstanceConnections = async (modelName, id, connectedModelName) => {
//     const key = modelName + "-" + connectedModelName;
//     if (!objectContains(MODEL_CONNECTION_TABLES, key))
//         throw new Error(`Could not find connection table for models: ${modelName}, ${connectedModelName}`);
//     const connectionTable = MODEL_CONNECTION_TABLES[key];
//     const modelAColumnName = MODEL_TO_CONNECTION_COLUMN_NAME[modelName];
//     const modelBColumnName = MODEL_TO_CONNECTION_COLUMN_NAME[connectedModelName];
//     const connectionData = await getFilteredInstances(connectionTable, { filters: [[modelAColumnName, "eq", id]] });
//     const connectionIds = connectionData.map(x => x[modelBColumnName]);
//     const data = await getFilteredInstances(connectedModelName, { filters: [["id", "in", connectionIds]] });
//     console.log("#DEBUG#")
//     console.log(data)
//     return data;
// };

export const getSingleInstanceConnections = async (modelName, id, connectedModelName) => {
    const key = `${modelName}-${connectedModelName}`;
    if (!objectContains(MODEL_CONNECTION_TABLES, key))
        throw new Error(`Could not find connection table for models: ${modelName}, ${connectedModelName}`);

    const url = makeURL(MODEL_CONNECTIONS_ENDPOINT, {"modelName": modelName, "id": id, "connectionName": connectedModelName});
    console.log(url);
    return await fetchJSON(url);
};

export const searchModelInstances = async (modelName, query) => {
    query = query.replace(" ", "_");
    const url = API_ENDPOINT_ROOT + `/${modelName}/search/nolimit?query=${query}`;

    console.log(url);
    const response = await fetchJSON(url);
    return response;
};

export const getSingleInstanceMedia = async (modelName, id, { media_src=null, media_type=null } = {}) => {
    if (media_src !== null && !ALLOWED_MEDIA_SRCS.includes(media_src))
        throw new Error(`Invalid media_src: ${media_src}`);
    if (media_type !== null && !ALLOWED_MEDIA_TYPES.includes(media_type))
        throw new Error(`Invalid media_type: ${media_type}`);
    let filters = [["model_type", "eq", modelName], ["model_id", "eq", id]];
    if (media_src)  filters.push(["media_src", "eq", media_src]);
    if (media_type) filters.push(["media_type", "eq", media_type]);
    return (await getFilteredInstances("media", { filters })).data;
};

export const getFilteredInstances = async (modelName, { skip=0, limit="ALL", sort="id", sortorder="ASC", filters=[] } = {}) => {
    const schema = await getSchema();
    if (!objectContains(schema, modelName))
        throw new Error(`Could not find model: ${modelName}`);
    if (!Array.isArray(filters))
        throw new Error(`Expected filters to be an array not ${filters}`);
    for (const filter of filters) {
        if (!Array.isArray(filter))
            throw new Error(`Expected single filter to be an array not ${filter}`);
        if (filter.length != 3)
            throw new Error(`Expected single filter array to have 3 elements not ${filter.length}`);
        let [col, op, val] = filter;
        if (!objectContains(schema[modelName], col))
            throw new Error(`Filter error: Could not find column ${col} in ${modelName}\nCulprit: ${filters}`);
        if (!ALLOWED_FILTER_OPERATORS.includes(op))
            throw new Error(`Invalid filter operator: ${op}`);
        if (op === "in" && !Array.isArray(val))
            throw new Error(`Filter error: expected array val for in operator not ${val}`);
    }

    let params = { modelName };
    if (skip)                   params.skip = skip;
    if (limit !== "ALL")        params.limit = limit;
    if (sort !== "id")          params.sort = sort;
    if (sortorder !== "ASC")    params.sortorder = sortorder;
    if (filters.length)         params.filters = JSON.stringify(filters);

    let url = makeURL(MODEL_ENDPOINT, params);
    console.log("URL: ", url);
    return await fetchJSON(url);
};

// I made this function for a previous personal project, figured it'd be helpful here
// Generic function to form url with query parameters
// https://www.google.com/{id} subtitutes params.id
// and adds the rest of the params as query parameters
export const makeURL = (baseURL, params) => {
    let paramsCopy = {...params};
    baseURL = baseURL.replace(/\{(.*?)\}/g, (_, p1) => {
        if (objectContains(paramsCopy, p1)) {
            let result = paramsCopy[p1];
            delete paramsCopy[p1];
            return result;
        }
        throw new Error(`Didn't find url parameter: ${p1} for ${baseURL} in ${JSON.stringify(params)}`);
    });

    let queryStr = new URLSearchParams();
    for (let key in paramsCopy) {
        if (!objectContains(paramsCopy, key)) continue;
        queryStr.append(key, paramsCopy[key]);
    }
    return baseURL + "?" + queryStr.toString();
};

const DEFAULT_FETCH_OPTIONS = {};
export const fetchJSON = async (url, options = {}) => {
    try {
        let res = await fetch(url, {...DEFAULT_FETCH_OPTIONS, ...options});
        return await res.json();
    } catch (err) {
        console.log(`An error occured while fetching ${url}\n${err.message}`);
    }
};

const objectContains = (obj, key) => {
    if (obj !== null && typeof(obj) === "object")
        return key in obj;
    return false;
};

export const findMedia = (media, media_src, media_type) => {
    if (!media) return null;
    const result = media.find((x) => x.media_src === media_src && x.media_type === media_type);
    if (!result) return null;
    return result.media_data;
};
