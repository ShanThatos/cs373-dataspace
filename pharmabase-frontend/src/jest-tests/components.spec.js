import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import ModelCard from "../components/pages/splash/ModelCard"
import HeroSection from "../components/pages/splash/HeroSection"
import NavigationBar from "../components/nav/NavigationBar"
import LinkCard from "../components/pages/about/LinkCard"
import MemberCard from "../components/pages/about/MemberCard"
import AttributeTable from "../components/global/tables/AttributeTable"
import ConnsCard from "../components/pages/instance/connections/ConnsCard"


configure({ adapter: new Adapter() });

describe("Testing splash page components", () => {
  test("Model cards should render correctly", () => {
    const component = shallow(<ModelCard
                                        name = "Test name"
                                        picture = "pic"
                                        link = "google.com"
                                        description = "Description of model"/>);

    expect(component).toMatchSnapshot();
  });

  test("Hero Section on should render correctly", () => {
    const component = shallow(<HeroSection/>);

    expect(component).toMatchSnapshot();
  });
});

describe("Testing about page components", () => {
  test("Link cards should render correctly", () => {
    const component = shallow(<LinkCard
                                        name = "Google"
                                        picture = "pic.jpg"
                                        link = "google.com"/>);

    expect(component).toMatchSnapshot();
  });

  test("Member cards should render correctly", () => {
    const image = jest.mock('../images/pharmabase-logo.png');
    const component = shallow(<MemberCard
      name = "Test name"
      bio = "Text about text"
      job = "Test job"
      photo = "photo.here"
      commits = {10}
      issues = {17}
      tests = {8}
      profilePhoto = {image}/>);

    expect(component).toMatchSnapshot();
  });
});

describe("Testing global components", () => {
  test("Navbar should render correctly", () => {
    const component = shallow(<NavigationBar/>);

    expect(component).toMatchSnapshot();
  });
});

describe("Testing instance page components", () => {
  test("Attribute Table should render correctly", () => {
    const instanceData = {
      'name' : 'Hydrogen',
      'atomic-number' : 1
    }
    const component = shallow(<AttributeTable
                                instanceData = {instanceData}/>);

    expect(component).toMatchSnapshot();
  });

  test("Attribute Table with no data shouold render", () => {
    const instanceData = {}
    const component = shallow(<AttributeTable
                                instanceData = {instanceData}/>);

    expect(component).toMatchSnapshot();
  });

  test("Connections card should render correctly", () => {
    const connections = [
      {
        name: 'tylenol',
        id: 1
      },
      {
        name: 'ibuprofen',
        id: 2
      },
      {
        name: 'deltrum',
        id: 3
      }
    ]
    const component = shallow(<ConnsCard
                                model = "drugs"
                                conns = {connections}/>);

    expect(component).toMatchSnapshot();
  });
});
