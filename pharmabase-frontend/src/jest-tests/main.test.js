/**
 * @jest-environment jsdom
 */


import React from "react";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { shallow, configure } from "enzyme";
import About from "../components/pages/about/About";
import Splash from "../components/pages/splash/Splash";
import Index from "../components/pages/index/Index";

configure({ adapter: new Adapter() });

describe("Render Non model pages", () => {
	test("About", () => {
		const aboutTest = shallow(<About />);
		expect(aboutTest).toMatchSnapshot();
	});

	test("Splash", () => {
		const splashTest = shallow(<Splash />);
		expect(splashTest).toMatchSnapshot();
	});
});
