export function camelToTitleCase(text){
  const result = text.replace(/([A-Z])/g, " $1");
  return result.charAt(0).toUpperCase() + result.slice(1);
}

export const capFirstChar = (text) => {
  return text.charAt(0).toUpperCase() + text.slice(1);
};



// make an object where the key is the element symbol and the value is it's weight
export const elementWeight = {
  H: 1.0079,
  He: 4.0026,
  Li: 6.941,
  Be: 9.0122,
  B: 10.811,
  C: 12.0107,
  N: 14.0067,
  O: 15.9994,
  F: 18.9984,
  Ne: 20.1797,
  Na: 22.9898,
  Mg: 24.305,
  Al: 26.9815,
  Si: 28.0855,
  P: 30.9738,
  S: 32.065,
  Cl: 35.453,
  Ar: 39.948,
  K: 39.0983,
  Ca: 40.078,
  Sc: 44.9559,
  Ti: 47.867,
  V: 50.9415,
  Cr: 51.9961,
  Mn: 54.938,
  Fe: 55.845,
  Co: 58.9332,
  Ni: 58.6934,
  Cu: 63.546,
  Zn: 65.38,
  Ga: 69.723,
  Ge: 72.64,
  As: 74.9216,
  Se: 78.96,
  Br: 79.904,
  Kr: 83.8,
  Rb: 85.4678,
  Sr: 87.62,
  Y: 88.9059,
  Zr: 91.224,
  Nb: 92.9064,
  Mo: 95.94,
  Tc: 98,
  Ru: 101.07,
  Rh: 102.9055,
  Pd: 106.42,
  Ag: 107.868,
  Cd: 112.411,
  In: 114.818,
  Sn: 118.71,
  Sb: 121.76,
  Te: 127.6,
  I: 126.9045,
  Xe: 131.293,
  Cs: 132.9055,
  Ba: 137.327,
  La: 138.
};

// get random color hashcode 
export const getRandomColor = () => {
  var letters = "0123456789ABCDEF";
  var color = "#";
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
};



// THIS IS SOME NASTY CODE BUT IT WORKS
// getPieChartData("C6HFe5C10")
export const getPieChartData = (formula) => {
  // example formula: C6HFe5Ca10

  const weights = {};

  // loop though each character in the formula string
  let elementString = "";
  let numString = "";
  for (let i = 0; i < formula.length; i++) {
    if (formula[i].match(/[A-Z]/) || formula[i].match(/[a-z]/)) {
      if (elementString.length > 0 && formula[i].match(/[A-Z]/)) {
        let weight = elementWeight[elementString];
        if (!(elementString in weights)) {
          weights[elementString] = 0;
        }
        weights[elementString] += weight;
        // console.log(elementString, weight);
        elementString = "";
        numString = "";
      } else if (numString.length > 0) {
        // check vals
        let weight = elementWeight[elementString] * parseInt(numString);
        if (!(elementString in weights)) {
          weights[elementString] = 0;
        }
        weights[elementString] += weight;
        // console.log(elementString, weight);
        elementString = "";
        numString = "";
      }
      elementString += formula[i];
    }
    // if the character is a number, add it to the weight string
    else if (formula[i].match(/[0-9]/)) {
      numString += formula[i];
    }
  }
  let weight = 0;
  if (numString.length > 0) {
    weight = elementWeight[elementString] * parseInt(numString);
  } else {
    weight = elementWeight[elementString];
  }
  if (!(elementString in weights)) {
    weights[elementString] = 0;
  }
  weights[elementString] += weight;
  // console.log(elementString, weight);

  // console.log(weights)
  // get the percentage of each elements weight in weights
  let pieChartData = [];

  // go though each object in weights
  Object.keys(weights).forEach((element) => {
    let weight = weights[element];
    pieChartData.push({
      name: element,
      value: weight,
      fill: getRandomColor(),
    });
  });


  return pieChartData;
};
