# Project PharmaBase

* GitLab repo: https://gitlab.com/ShanThatos/cs373-dataspace/
* Website: https://www.pharmabase.me/
* API: https://api.pharmabase.me
* API Docs: https://docs.pharmabase.me


* Group Number: 10 AM Group 4

* Team Members:
    - Earl Potts (elp947) @earlpotts
    - Luca Chaves Rodrigues Noronha (ls42578) @luca.santos
    - Peter Ma (phm387) @peterhma
    - Shanth Koka (ssk2578) @ShanThatos
    - Samson Broten (srb3846) @brotensam


### Time to Completion (E=Estimate, A=Actual)
| Team Member                       | P1E | P1A | P2E | P2A | P3E | P3A | P4E | P4A |
| -----------                       | --- | --- | --- | --- | --- | --- | --- | --- |
| Earl Potts                        | 15  | 15  | 20  | 21  | 20  | 27  |  5  |  5  |
| Luca Chaves Rodrigues Norononha   | 15  | 15  | 20  | 19  | 22  | 25  |  6  |  7  |
| Peter Ma                          | 14  | 12  | 20  | 24  | 20  | 22  |  8  |  8  |
| Samson Broten                     | 15  | 15  | 20  | 19  | 20  | 23  |  5  |  5  |
| Shanth Koka                       | 13  | 13  | 20  | 25  | 25  | 26  |  6  |  6  |


* Project Name: PharmaBase

* Important Links:
    - [GitLab Repository](https://gitlab.com/ShanThatos/cs373-dataspace)
    - [Pharmabase Hosted Website](https://www.pharmabase.me/)
    - [Pharmabase API](https://api.pharmabase.me/)
    - [Pharmabase Docs](http://docs.pharmabase.me)

| Phases        | Project Leader        | GitLab Pipeline       | GitLab SHA    |
| ------        | ----                  | ----                  | ----          |
| Phase 1       | Shanth Koka           | [Phase 1 Pipeline](https://gitlab.com/ShanThatos/cs373-dataspace/-/commit/8a01d6d19a2430e20715371617d9428cb0d9d0fa) | 8a01d6d19a2430e20715371617d9428cb0d9d0fa |
| Phase 2       | Luca Santos           | [Phase 2 Pipeline](https://gitlab.com/ShanThatos/cs373-dataspace/-/pipelines/517413793) | 451c241fa975115e0eabeaa1f2c3fd6f27450749 |
| Phase 3       | Shanth Koka           | [Phase 3 Pipeline](https://gitlab.com/ShanThatos/cs373-dataspace/-/pipelines/517413793) | 451c241fa975115e0eabeaa1f2c3fd6f27450749 |
| Phase 4       | Peter Ma           | [Phase 4 Pipeline](https://gitlab.com/ShanThatos/cs373-dataspace/-/pipelines/532651854) | a84ecd442a5db897a46bee902ce5158fb16ea850 |



* Comments
    We added our presentation video to the bottom of our Splash page. 

* Presentation Video
    [Youtube Link](https://www.youtube.com/watch?v=EsvgmoQQlNE)


* Project Proposal:
    PharmaBase is an intuitive tool that provides an easy avenue to learn more about the makeup of pharmaceutical drugs.
    Many of us in the modern world take medicines and supplements to better our health, however we are often not as informed on the makeup of these drugs, and the interesting chemistry that goes into them. PharmaBase will have a database of the most common and popular pharmaceutical drugs, and will describe the drugs as well as the chemical compounds that makeup the active ingredients. 
    We also will provide info on the chemical elements that make up the compounds themselves, to connect the complexities of pharmaceuticals to the basics of chemistry.
    Finally, our project will be different from other sites that are thematically similar due to its educational focus and impartiality, rather than attempting to push certain drugs or compounds to customers.

* Data Sources:
    - https://open.fda.gov/apis/drug/ndc/example-api-queries/
    - https://pubchemdocs.ncbi.nlm.nih.gov/compounds
    - https://chembl.gitbook.io/chembl-interface-documentation/web-services/chembl-data-web-services
    - https://github.com/neelpatel05/periodic-table-api
    - https://www.mediawiki.org/wiki/API:Main_page
    - https://storage.googleapis.com/search-ar-edu/periodic-table/element_ATOMICNUM_ELEMENTNAME/element_ATOMICNUM_ELEMENTNAME.glb

* Models:
    - Drugs
    - Compounds
    - Elements 

![](/pharmabase-backend/schema/schema.png)

* Model Attributes:
    - Filterable/Sortable:
        - Drugs
            - Marketing Status (discontinued, prescription, none — tentative approval)
            - Dosage Form (tablet, solution, etc.)
            - First submission date
            - Most recently update date
            - Product Type (OTC or Prescription)
            - Route (of administration; e.g. oral, intravenous, etc)
        - Compounds
            - Freezing point
            - Boiling Point
            - Molecular Mass 
            - Heavy (non-hydrogen) atom count
            - Hydrogen Bond Donor Count
            - Hydrogen Bond Acceptor Count
        - Elements
            - Atomic Radius
            - Boiling Point
            - Year Discovered
            - Ion Radius
            - Ionization Energy
    - Searchable:
        - Drugs
            - Generic name (List | e.g. Acetaminophen. Some drugs have > 1 names.)
            - SPL_ID
            - Brand name (List |  e.g. Tylenol, Advil, etc.)
            - Manufacturer names (List | e.g. McNeil, Wyeth, etc.)
            - Active ingredients (List | e.g. Acetaminophen, etc.)
        - Compounds
            - IUPAC Name
            - IUPAC International Chemical Identifier (InChI)
            - IUPAC InChI Key
            - Molecular Formula
            - Pubchem ID3
        - Elements
            - Generic name
            - Element Symbol
            - Atomic Number
            - Standard State (solid, liquid, gas)
            - Element Classification (Group Block: Alkali metal, noble gas, …)

* Model Connections:
    - Drugs are made of compounds, and containing particular elements
    - Compounds make up drugs and are made from individual elements
    - Elements make up molecules and are found in drugs

* Model Media:
    - Molecular structure of Drugs and Compounds
    - Raw element pictures
    - Sample picture of Drug as a product
    - Wikipedia images/text of drugs, compounds, and elements

* Questions it will answer:
    - What compounds and elements make up a drug?
    - What company manufactures a drug? 
    - What are the active ingredients in this drug? 
    - When was this drug last updated? 
    - Which drugs have glucose in it? 
    - Which drugs/compounds contain potassium?
