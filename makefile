
BACKEND_DIR := pharmabase-backend/
FRONTEND_DIR := pharmabase-frontend/
POSTMAN_TESTS_DIR := postman-tests/
SELENIUM_TESTS_DIR := selenium-tests/

run-backend:
	$(MAKE) -C $(BACKEND_DIR) -s install
	$(MAKE) -C $(BACKEND_DIR) -s test
	$(MAKE) -C $(BACKEND_DIR) -s run

run-frontend:
	$(MAKE) -C $(FRONTEND_DIR) -s install
	$(MAKE) -C $(FRONTEND_DIR) -s test
	$(MAKE) -C $(FRONTEND_DIR) -s run

run-postman-tests:
	$(MAKE) -C $(POSTMAN_TESTS_DIR) -s install
	$(MAKE) -C $(POSTMAN_TESTS_DIR) -s test

run-selenium-tests:
	$(MAKE) -C $(SELENIUM_TESTS_DIR) -s install
	$(MAKE) -C $(SELENIUM_TESTS_DIR) -s test

install-all:
	$(MAKE) -C $(BACKEND_DIR) -s install
	$(MAKE) -C $(FRONTEND_DIR) -s install
	$(MAKE) -C $(POSTMAN_TESTS_DIR) -s install
	$(MAKE) -C $(SELENIUM_TESTS_DIR) -s install

test-all:
	-$(MAKE) -C $(BACKEND_DIR) -s test
	-$(MAKE) -C $(FRONTEND_DIR) -s test
	-$(MAKE) -C $(POSTMAN_TESTS_DIR) -s test
	-$(MAKE) -C $(SELENIUM_TESTS_DIR) -s test