import requests
import os
import json


def fetchJSON(url):
    return requests.get(url).json()


def convertToMapByID(data):
    return {item["id"]: item for item in data}


def convertToMapOfLists(data, key):
    ret = {}
    for item in data:
        ret[item[key]] = ret.get(item[key], []) + [item]
    return ret

# pie chart data for element usage for all drugs


def vis1(drugs, drugelement, elements):
    data = {}
    drugelementMap = convertToMapOfLists(drugelement, "drugs_id")
    elementsMap = convertToMapByID(elements)
    for drug in drugs:
        elementIDs = [x["elements_id"] for x in drugelementMap[drug["id"]]]
        for elementID in elementIDs:
            element = elementsMap[elementID]
            d = data.get(element["id"], {"count": 0})
            d["count"] += 1
            d["name"] = element["name"]
            d["symbol"] = element["symbol"]
            data[element["id"]] = d

    data = list(data.values())

    json.dump(data, open(
        "../pharmabase-frontend/src/components/pages/visualizations/pharmabase/data/vis1.json", "w"), indent=4)


def vis2(drugs, drugcompound, compounds):
    counts = {
        "oral": {},
        "intravenous": {},
        "inhalation": {},
        "subcutaneous": {},
    }

    compoundsMap = convertToMapByID(compounds)

    def checkAttributes(roa, compound_id):
        for attribute, val in attributes.items():
            if attribute not in counts[roa]:
                counts[roa][attribute] = 0
            counts[roa][attribute] += int(compoundsMap[compound_id]
                                          [attribute]) <= val
        if "total" not in counts[roa]:
            counts[roa]["total"] = 0
        counts[roa]["total"] += 1

    attributes = {"molecular_weight": 500.0,
                  "hydrogen_bond_donor_count": 5,
                  "hydrogen_bond_acceptor_count": 10,
                  "rotatable_bond_count": 10}

    axis_names = {"molecular_weight": "Mass ≤ 500.0 Da",
                  "hydrogen_bond_donor_count":  " ≤ 5 H-bond donors",
                  "hydrogen_bond_acceptor_count": "≤ 10 H-bond acceptors ",
                  "rotatable_bond_count": "≤ 10 Rotatable bonds"}

    drugcompoundMap = convertToMapOfLists(drugcompound, "drugs_id")

    for drug in drugs:
        roa = drug["route_of_administration"].lower()
        for key in counts.keys():
            if key in roa:
                for c in drugcompoundMap[drug["id"]]:
                    checkAttributes(key, c["compounds_id"])

    # print(json.dumps(counts, indent=4))
    data = []

    for roa, attributeList in counts.items():
        route_of_admin = [{"axis": axis_names[attribute], "value": round(attributeList[attribute] /
                           attributeList["total"], 2)} for attribute in attributes.keys()]

        data.append(route_of_admin)
    # print(json.dumps(data, indent=4))

    json.dump(data, open(
        "../pharmabase-frontend/src/components/pages/visualizations/pharmabase/data/vis2.json", "w"), indent=4)


def vis3(elements):

    def builb_mock_date(d):
        return "01-01-"+str(d)

    data = [{"label": el["symbol"], "value": builb_mock_date(
        el["year_discovered"])} for el in elements if el["year_discovered"] != 0]

    json.dump(data, open(
        "../pharmabase-frontend/src/components/pages/visualizations/pharmabase/data/vis3.json", "w"), indent=4)


def main():
    if not os.path.exists("./data/"):
        os.makedirs("./data/")

    drugs = fetchJSON("https://api.pharmabase.me/drugs")["data"]
    compounds = fetchJSON("https://api.pharmabase.me/compounds")["data"]
    elements = fetchJSON("https://api.pharmabase.me/elements")["data"]
    drugcompound = fetchJSON("https://api.pharmabase.me/drugcompound")["data"]
    drugelement = fetchJSON("https://api.pharmabase.me/drugelement")["data"]
    compoundelement = fetchJSON(
        "https://api.pharmabase.me/compoundelement")["data"]

    json.dump(drugs, open("./data/drugs.json", "w"), indent=4)
    json.dump(compounds, open("./data/compounds.json", "w"), indent=4)
    json.dump(elements, open("./data/elements.json", "w"), indent=4)
    json.dump(drugcompound, open("./data/drugcompound.json", "w"), indent=4)
    json.dump(drugelement, open("./data/drugelement.json", "w"), indent=4)
    json.dump(compoundelement, open(
        "./data/compoundelement.json", "w"), indent=4)

    # vis1(drugs, drugelement, elements)
    vis2(drugs, drugcompound, compounds)
    # vis3(elements)


if __name__ == "__main__":
    main()
