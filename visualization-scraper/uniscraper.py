import requests
import os
import json

def fetchJSON(url):
    return requests.get(url).json()

def convertToMapByID(data):
    return {item["id"]: item for item in data}

# thinking of getting average college cost per state
def vis1(colleges, cities):
    cityMap = convertToMapByID(cities)

    collegeCosts = {}
    for college in colleges:
        state = cityMap[college["cityId"]]["state"]
        sumCost, numColleges = collegeCosts.get(state, (0, 0))
        collegeCosts[state] = (sumCost + college["avgCost"], numColleges + 1)

    data = {state: round(cost / numColleges, 2) for state, (cost, numColleges) in collegeCosts.items()}
    data = [{"label": state, "value": cost} for state, cost in data.items()]
    data.sort(key=lambda x: x["value"])

    json.dump(data, open("./data/vis1.json", "w"), indent=4)
    json.dump(data, open("../pharmabase-frontend/src/components/pages/visualizations/univercity/data/vis1.json", "w"), indent=4)

def vis2(companies):
    data = [{"label": x["name"], "value": x["annualRevenue"], "logo": x["iconUrl"]} for x in companies if x["annualRevenue"]]
    data.sort(key=lambda x: x["value"], reverse=True)

    json.dump(data, open("./data/vis2.json", "w"), indent=4)
    json.dump(data, open("../pharmabase-frontend/src/components/pages/visualizations/univercity/data/vis2.json", "w"), indent=4)

def vis3(colleges):
    data = [{"name": x["name"], "admissionRate": int(x["admissionRate"] * 100), "satAvg": x["satAvg"]} for x in colleges]

    json.dump(data, open("./data/vis3.json", "w"), indent=4)
    json.dump(data, open("../pharmabase-frontend/src/components/pages/visualizations/univercity/data/vis3.json", "w"), indent=4)

def main():
    if not os.path.exists("./data/"):
        os.makedirs("./data/")
    companies = fetchJSON(f"https://api.univercity.me/companies?pageSize=100000000")["data"]["companies"]
    colleges = fetchJSON(f"https://api.univercity.me/colleges?pageSize=100000000")["data"]["colleges"]
    cities = fetchJSON(f"https://api.univercity.me/cities?pageSize=100000000")["data"]["cities"]
    
    json.dump(companies, open("./data/companies.json", "w"), indent=4)
    json.dump(colleges, open("./data/colleges.json", "w"), indent=4)
    json.dump(cities, open("./data/cities.json", "w"), indent=4)

    vis1(colleges, cities)
    vis2(companies)
    vis3(colleges)


if __name__ == "__main__":
    main()