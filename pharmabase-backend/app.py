import os
import sys

from dotenv import load_dotenv
from flask import Flask, abort, jsonify, redirect, request

from extensions import *
from models import *
from scraper.mediascraper import *
from scraper.wordBankMaker import *
from scraper.textBankMaker import *
from scraper.crudesearch.indexer import getIndex, search


load_dotenv()

app = Flask(__name__)

app.config["SQLALCHEMY_ENGINE_OPTIONS"] = {"pool_pre_ping": True}
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get("DATABASE_URL")
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_ECHO"] = False
app.config["JSONIFY_PRETTYPRINT_REGULAR"] = True
app.config["JSON_SORT_KEYS"] = False

db.app = app
db.init_app(app)


@app.before_first_request
def before_first():
    try:
        global drugsBank, elementsBank, compoundsBank, drugsTextBank, elementsTextBank, compoundsTextBank
        allBanks = loadWordBanks()
        drugsBank, compoundsBank, elementsBank = allBanks
        allTextBanks = loadTextBanks()
        drugsTextBank, compoundsTextBank, elementsTextBank = allTextBanks
    except Exception as e:
        print(e)


@app.before_request
def enforce_https():
    if not app.testing and not app.debug and not request.is_secure:
        return redirect(request.url.replace("http://", "https://", 1), code=301)


@app.after_request
def add_cors_headers(response):
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Credentials"] = "true"
    return response


@app.route("/")
def index():
    return f"<h1>Welcome to the Pharmabase API!</h1><a href='/docs' target='_blank'>View Documentation</a>"


@app.route("/docs")
def docs():
    return redirect("https://documenter.getpostman.com/view/19991215/UVyyutJH")


FILTER_OPERATORS = {
    "eq": lambda key, val: f"LOWER('' || {key}) = LOWER('{val}')",
    "noteq": lambda key, val: f"LOWER('' || {key}) <> LOWER('{val}')",
    "like": lambda key, val: f"{key} ILIKE '%%{val}%%'",
    "lessthan": lambda key, val: f"{key} < '{val}'",
    "lessthaneq": lambda key, val: f"{key} <= '{val}'",
    "greaterthan": lambda key, val: f"{key} > '{val}'",
    "greaterthaneq": lambda key, val: f"{key} >= '{val}'",
    "in": lambda key, val: "FALSE"
    if not val
    else f"{key} IN (%s)" % ",".join(f"'{v}'" for v in val),
}


@app.route("/<string:model>")
def api_v2_models(model):
    if model not in [x.__tablename__ for x in ALL_MODELS]:
        abort(404)
    args = dict(request.args)

    skip = int(args.get("skip", 0))
    limit = args.get("limit", "ALL")
    limit = int(limit) if limit.isdigit() else limit
    filters = json.loads(args.get("filters", "[]"))
    sort = str(args.get("sort", "id"))
    sortorder = str(args.get("sortorder", "ASC"))

    modelSchema = getModelSchema(model)
    filters.extend([arg, "eq", args[arg]] for arg in args if arg in modelSchema)

    where = [
        FILTER_OPERATORS[op.strip()](
            key.strip(), val.strip() if isinstance(val, str) else val
        )
        for key, op, val in filters
    ] or ["TRUE"]
    where = " AND ".join(f"({x})" for x in where)

    ret = {"model": model}
    ret["total_count"] = executeRawSQL(f"SELECT COUNT(*) FROM {model} WHERE {where}")[
        0
    ]["count"]
    if "count" not in args:
        ret["data"] = executeRawSQL(
            f"SELECT * FROM {model} WHERE {where} ORDER BY {sort} {sortorder} LIMIT {limit} OFFSET {skip}"
        )
    return jsonify(ret)


@app.route("/connections/<string:model>/<int:id>/<string:connectedModel>")
def api_v2_model_connections(model, id, connectedModel):
    if model not in [x.__tablename__ for x in ALL_MODELS] or connectedModel not in [
        x.__tablename__ for x in ALL_MODELS
    ]:
        abort(404)
    middleModel = CONNECTED_MODEL_MAP[model + "-" + connectedModel].__tablename__

    return jsonify(
        executeRawSQL(
            f"SELECT * FROM {middleModel} a, {connectedModel} b WHERE a.{model}_id = '{id}' AND a.{connectedModel}_id = b.id"
        )
    )


@app.route("/schema")
def api_v2_schema():
    return jsonify({x.__tablename__: getModelSchema(x) for x in ALL_MODELS})


@app.route("/<string:model>/search/<string:limit>")
def api_v2_model_search(model, limit):
    # TODO make limit as an arg instead of a path
    if model not in [x.__tablename__ for x in ALL_MODELS] and not model == "all":
        abort()
    args = dict(request.args)
    searchQuery = str(args.get("query"))  # will be delimited by "_"

    # boundaries
    mapping = {
        "drugs": {
            "b": drugsBank,
            "tb": drugsTextBank,
            "c": Drug.getSearchableColumns(),
        },
        "compounds": {
            "b": compoundsBank,
            "tb": compoundsTextBank,
            "c": Compound.getSearchableColumns(),
        },
        "elements": {
            "b": elementsBank,
            "tb": elementsTextBank,
            "c": Element.getSearchableColumns(),
        },
    }

    res = findMatchingModelIDs(
        model=model,
        limit=limit,
        queryString=searchQuery,
        bank=mapping[model]["b"],
        textBank=mapping[model]["tb"],
        columnNames=mapping[model]["c"],
    )
    #    return json.dumps(res, indent=4, default=str, sort_keys=False)
    return jsonify(res)


@app.route("/crudesearch")
def api_v2_crude_search():
    query = request.args.get("query")
    if not query:
        return "No query provided"
    query = query.lower()
    models = [
        x.strip()
        for x in request.args.get("models", "drugs, compounds, elements").split(",")
    ]
    limit = int(request.args.get("limit", "-1"))

    return jsonify(search(query, models, limit))


drugsBank, compoundsBank, elementsBank = None, None, None
drugsTextBank, compoundsTextBank, elementsTextBank = None, None, None

if __name__ == "__main__":
    if "--mediaScrape" in sys.argv:
        mediaScrape()
    if "--remakeDB" in sys.argv:
        remakeDatabase()
        clearDisconnectedData()
        mediaInsert()
    if "--clearDisconnects" in sys.argv:
        clearDisconnectedData()
    if "--remakeMedia" in sys.argv:
        mediaInsert()
    if "--remakeWordBanks" in sys.argv:  # Remake the pickle file
        remakeWordBanks()
    if (
        "--remakeTextBanks" in sys.argv
    ):  # takes a pretty long time. Not many reasons to do this.
        remakeTextBanks()
    if "--generateUML" in sys.argv:
        generateUML()
    if "--generateIndex" in sys.argv:
        getIndex()
    if "--runServer" in sys.argv:
        allBanks = loadWordBanks()
        drugsBank, compoundsBank, elementsBank = allBanks
        allTextBanks = loadTextBanks()
        drugsTextBank, compoundsTextBank, elementsTextBank = allTextBanks
        app.run(host="0.0.0.0", debug=True)
