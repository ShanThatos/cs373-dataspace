import json
import requests
from typing import Any, List
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy_utils import force_instant_defaults

from collections import OrderedDict

db = SQLAlchemy()
wordBank = {}


def _fix(arg):
    if type(arg) is str and arg.isnumeric():
        return int(arg)
    elif type(arg) is list:
        return [_fix(x) for x in arg]
    elif type(arg) is dict:
        return {_fix(x): _fix(y) for x, y in arg.items()}
    return arg


def dictify(result):
    return [{_fix(x[0]): _fix(x[1]) for x in row._mapping.items()} for row in result]


def executeRawSQL(query: str, expectData=True):
    print("Executing Raw SQL:", query)
    if expectData:
        return dictify(db.engine.execute(query))
    db.engine.execute(query)


LOG_PREFIX = "Pharmabase Log:"


def log(*args: List[Any]):
    print(LOG_PREFIX, *args)


# Useful for scraping
def fetchJSON(url: str):
    response = requests.get(url)
    return response.json()


def writeToFile(fileName: str, data, indent=4):
    # os.makedirs(os.path.dirname(fileName), exist_ok=True)
    with open(fileName, "w") as writeFile:
        json.dump(data, writeFile, indent=indent)


def findMatchingModelIDs(model, limit, queryString, bank, textBank, columnNames):
    omit = ["the", "is"]
    limit = None if limit == "nolimit" else int(limit)
    words = queryString.split("_")
    limitSQL = "" if limit == None else ("LIMIT " + str(limit))

    cols = ", ".join(columnNames)

    def searchTables():
        # Using SIMILAR TO is alot shorter but is slower than ILIKE, so that's why I did all of this concatentaiton
        col_ILIKE = " OR ".join(
            [
                f"CAST({colName} AS TEXT) ILIKE '%%{word}%%'"
                for colName in columnNames
                for word in words
            ]
        )
        prim_case = "+".join(
            [
                f"(CASE WHEN CAST({colName} AS TEXT) ILIKE '%%{word}%%' THEN 1 ELSE 0 END)"
                for colName in columnNames
                for word in words
            ]
        )
        sec_case = "+".join(
            [
                f"(CASE WHEN CAST({colName} AS TEXT) ILIKE '%%{queryString}%%' THEN 2 ELSE 0 END)"
                for colName in columnNames
            ]
        )
        case = prim_case + " + " + sec_case

        sqlRes = executeRawSQL(
            f"SELECT {cols} FROM {model} WHERE {col_ILIKE} ORDER BY {case} DESC {limitSQL}"
        )

        result = {}
        for instance in sqlRes:
            result[instance["id"]] = {
                "name": instance["name"],
                "preview_text": instance,
            }
        return result

    # will refactor this later to be cleaner
    def addToDict(d, id, name, match, n):
        try:
            d[id]["unique_matches"].update(match)
            d[id]["num_occurences"] += n
        except:
            d[id] = {"unique_matches": [match], "num_occurences": n, "name": name}

    def searchTextMedia(resultLimit):
        matchingModelIDs = {}
        for i in range(len(words)):
            targetWord = words[i]
            if targetWord not in omit and targetWord in bank:
                for modelID in bank[targetWord]:
                    if modelID not in textBank or not textBank[modelID]["modelName"]:
                        continue
                    instance = bank[targetWord][modelID]
                    addToDict(
                        matchingModelIDs,
                        modelID,
                        textBank[modelID]["modelName"][0]["name"],
                        (i, instance["firstOcc"]),
                        instance["occurences"],
                    )

        matchingModelIDs = OrderedDict(
            sorted(
                matchingModelIDs.items(),
                key=lambda item: item[1]["num_occurences"],
                reverse=True,
            )[:resultLimit]
        )
        result = {}
        if len(matchingModelIDs):

            for id, v in matchingModelIDs.items():
                text = textBank[id]["modelText"]
                matchList = sorted(
                    v["unique_matches"], key=lambda item: [1], reverse=True
                )
                lastTuple = matchList[len(matchList) - 1]  # last in the text media

                firstInd = matchList[0][1]
                lastInd = lastTuple[1] + len(
                    words[lastTuple[0]]
                )  # last index of the last term of the text media to present in the preview

                leftOffset = max(firstInd - 40, 0)
                rightOffset = min(lastInd + 40, len(text) - 1)
                leftPrefix = "..." if leftOffset > 0 else ""
                rightPrefix = "..." if rightOffset < len(text) - 1 else ""

                previewText = leftPrefix + text[leftOffset:rightOffset] + rightPrefix
                result[id] = {"name": v["name"], "preview_text": previewText}
            sqlRes = executeRawSQL(
                f"SELECT {cols} FROM {model} "
                + "WHERE id IN ({})".format(
                    ",".join([str(id) for id in matchingModelIDs.keys()])
                )
            )
            for instance in sqlRes:
                preview = result[instance["id"]].get("preview_text")
                instance["relevant_excerpt"] = preview
                result[instance["id"]].update({"preview_text": instance})

        # [result[instance["id"]].update({"preview_text" : instance +  "\n\n" + (result[instance["id"]].get("preview_text"))}) for instance in sqlRes]
        return result

    finalRes = {}
    tableResults = searchTables()
    if not limit:
        textMediaResults = searchTextMedia(None)
        finalRes = {**tableResults, **textMediaResults}
    elif limit and len(tableResults) < limit:
        textMediaResults = searchTextMedia(limit - len(tableResults))
        finalRes = {**tableResults, **textMediaResults}

    return list(finalRes.values())
