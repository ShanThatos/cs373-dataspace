import pytest
import functools

from app import app
from models import ALL_MODELS

app.testing = True


def get_text(url):
    def decorator(f):
        @functools.wraps(f)
        def wrapper():
            with app.test_client() as client:
                response = client.get(url)
                text = response.get_data(as_text=True)
                f(response=text)

        return wrapper

    return decorator


def get_json(url):
    def decorator(f):
        @functools.wraps(f)
        def wrapper():
            with app.test_client() as client:
                if client is None:
                    print("client is None")
                response = client.get(url)
                if response is None:
                    print("response is None")
                json = response.json
                f(response=json)

        return wrapper

    return decorator


def get_jsons(*urls):
    def decorator(f):
        @functools.wraps(f)
        def wrapper():
            with app.test_client() as client:
                response = [client.get(url).json for url in urls]
                f(response=response)

        return wrapper

    return decorator


def test_app_exists():
    assert app


@get_text("/")
def test_app_index(response=None):
    assert "View Documentation" in response


@get_json("/schema")
def test_schema_tables(response=None):
    for table in response:
        assert table in [x.__tablename__ for x in ALL_MODELS]


@get_json("/schema")
def test_schema_fields(response=None):
    for table_schema in response.values():
        for field_type in table_schema.values():
            assert field_type["type"] in "INTEGER NUMERIC FLOAT DATE TEXT".split(" ")


@get_json("/drugs?id=1")
def test_query_by_id_drugs(response=None):
    assert response["model"] == "drugs"
    assert response["total_count"] == 1
    assert len(response["data"]) == 1
    assert response["data"][0]["name"] == "METOLAZONE"


@get_json("/compounds?id=15")
def test_query_by_id_compounds(response=None):
    assert response["model"] == "compounds"
    assert response["total_count"] == 1
    assert len(response["data"]) == 1
    assert response["data"][0]["name"] == "GATIFLOXACIN"


@get_json("/elements?id=6")
def test_query_by_id_elements(response=None):
    assert response["model"] == "elements"
    assert response["total_count"] == 1
    assert len(response["data"]) == 1
    assert response["data"][0]["name"] == "Carbon"


@get_jsons("/elements?skip=5&limit=10", "/elements?skip=8&limit=5")
def test_pagination(response=None):
    res1, res2 = response
    assert res1["total_count"] == res2["total_count"] == 40
    assert res1["model"] == res2["model"] == "elements"
    assert len(res1["data"]) == 10
    assert len(res2["data"]) == 5
    assert all(a == b for a, b in zip(res1["data"][3:8], res2["data"]))


@get_json("""/elements?filters=[["group_block","eq","nonmetal"]]""")
def test_eq_filter(response=None):
    assert response["total_count"] == 7
    assert len(response["data"]) == 7
    assert all(x["group_block"] == "nonmetal" for x in response["data"])


@get_json("""/elements?filters=[["group_block","noteq","nonmetal"]]""")
def test_noteq_filter(response=None):
    assert response["total_count"] == 33
    assert len(response["data"]) == 33
    assert all(x["group_block"] != "nonmetal" for x in response["data"])


@get_json("""/elements?filters=[["group_block","like","metal"]]""")
def test_like_filter(response=None):
    assert response["total_count"] == 32
    assert len(response["data"]) == 32
    assert all("metal" in x["group_block"] for x in response["data"])


@get_json("""/elements?filters=[["symbol","in",["K","H","O","C"]]]&sort=symbol""")
def test_in_filter(response=None):
    assert response["total_count"] == 4
    assert len(response["data"]) == 4
    assert all("metal" in x["group_block"] for x in response["data"])
    assert all(a["symbol"] == b for a, b in zip(response["data"], "C H K O".split()))


@get_json("""/connections/drugs/1/elements""")
def test_connections(response=None):
    assert len(response) == 6
    assert set(x["symbol"] for x in response) == {"H", "C", "N", "O", "S", "Cl"}


@get_json("""/drugs/search/nolimit?query=diabetic""")
def test_search_drugs(response=None):
    for drug in response:
        assert "diabetic" in str(drug["preview_text"]).lower()


@get_json("""/compounds/search/nolimit?query=C26H52N6O11S""")
def test_search_drugs(response=None):
    for drug in response:
        assert "C26H52N6O11S".lower() in str(drug["preview_text"]).lower()
