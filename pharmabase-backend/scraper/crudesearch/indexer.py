import os
import pickle
import re

from extensions import db, log, executeRawSQL, fetchJSON, writeToFile
from models import *

CACHE_DIR = "./scraper/crudesearch/cache/"
MODEL_TEXT_SQL = """SELECT d.id, d.name, ('' || ARRAY_TO_STRING(ARRAY(SELECT media_data FROM media m WHERE m.model_type = '%s' AND m.model_id = d.id AND m.media_type = 'raw_text'), ' ') || ROW_TO_JSON(d)) AS text FROM %s d"""


def saveCache(filename, data):
    if not os.path.exists(CACHE_DIR):
        os.makedirs(CACHE_DIR)
    with open(CACHE_DIR + filename, "wb") as handle:
        pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)


def loadCache(filename):
    if not os.path.exists(CACHE_DIR):
        os.makedirs(CACHE_DIR)
    if not os.path.exists(CACHE_DIR + filename):
        return None
    with open(CACHE_DIR + filename, "rb") as handle:
        return pickle.load(handle)


LOADED_MODEL_TEXTS = None


def getAllModelTexts():
    global LOADED_MODEL_TEXTS
    if LOADED_MODEL_TEXTS:
        return LOADED_MODEL_TEXTS
    LOADED_MODEL_TEXTS = loadCache("all_model_texts.pkl")

    if LOADED_MODEL_TEXTS:
        return LOADED_MODEL_TEXTS
    data = {}
    for model in [Drug, Compound, Element]:
        model_type = model.__tablename__
        result = executeRawSQL(MODEL_TEXT_SQL % (model_type, model_type))
        data[model_type] = {}
        for row in result:
            data[model_type][row["id"]] = {
                "name": row["name"],
                "text": " "
                + re.sub(
                    "[^a-z0-9]+", " ", row["text"].lower().replace("'", "")
                ).strip()
                + " ",
            }
    LOADED_MODEL_TEXTS = data
    saveCache("all_model_texts.pkl", data)
    return data


def transformTextsToIndex(texts):
    index = {}
    for model, modelTexts in texts.items():
        for modelId, modelData in modelTexts.items():
            text = modelData["text"]
            instanceKey = (model, modelId)
            words = text.split()
            for wi, word in enumerate(words):
                wordIndex = index.get(word, {})
                succeedingWords = wordIndex.get(instanceKey, []) + (
                    [words[wi + 1]] if wi < len(words) - 1 else []
                )
                wordIndex[instanceKey] = succeedingWords
                index[word] = wordIndex
    for word, wordIndex in index.items():
        for instanceKey, succeedingWords in wordIndex.items():
            index[word][instanceKey] = set(succeedingWords)
    return index


LOADED_INDEX = None


def getIndex():
    global LOADED_INDEX
    if LOADED_INDEX:
        return LOADED_INDEX
    LOADED_INDEX = loadCache("index.pkl")
    if LOADED_INDEX:
        return LOADED_INDEX
    texts = getAllModelTexts()
    LOADED_INDEX = transformTextsToIndex(texts)
    saveCache("index.pkl", LOADED_INDEX)


def search(query, models=["drugs", "compounds", "elements"], limit=-1):
    words = query.split()
    results = []
    used = {}
    for numWords in range(len(words), 0, -1):
        for i in range(len(words) - numWords + 1):
            print(" ".join(words[i : i + numWords]))
            # matches = searchWords(" ".join(words[i:i + numWords]), models=models)
            matches = searchWordsIndex(words[i : i + numWords], models)
            for result in matches:
                key = (result["model"], result["name"])
                score = numWords * sum(
                    len(word) ** 2 for word in words[i : i + numWords]
                )
                if key in used and used[key] > score:
                    continue
                used[key] = score
                results.append({**result, "score": score})
                if limit > 0 and len(results) >= limit * 5:
                    break
            else:
                continue
            break
        else:
            continue
        break
    results.sort(key=lambda x: x["score"], reverse=True)

    cleanResults = []
    used = set()
    for result in results:
        key = (result["model"], result["name"])
        if key in used:
            continue
        used.add(key)
        cleanResults.append({k: v for k, v in result.items() if k != "score"})
    return cleanResults[:limit]


def searchWords(words, models=["drugs", "compounds", "elements"]):
    # index = getIndex()

    # Crude search -- doesn't use special index data structure
    modelTexts = getAllModelTexts()
    for model, modelData in modelTexts.items():
        if model not in models:
            continue
        for modelId, instanceData in modelData.items():
            text = instanceData["text"]
            location = text.find(" " + words.strip() + " ")
            if location >= 0:
                preview = text[max(location - 50, 0) : min(location + 50, len(text))]
                preview = preview.replace(
                    " " + words.strip() + " ", " <b>" + words.strip() + "</b> "
                )
                yield {
                    "model": model,
                    "id": modelId,
                    "name": instanceData["name"],
                    "preview": preview,
                }


def searchWordsIndex(words, models=["drugs", "compounds", "elements"]):
    wordsstr = " ".join(words)
    index = getIndex()
    matches = set(x for x in index.get(words[0], {}).keys() if x[0] in models)
    for i in range(0, len(words) - 1):
        currentWord = words[i]
        nextWord = words[i + 1]
        matches.intersection_update(
            k
            for k, v in index.get(currentWord, {}).items()
            if k[0] in models and nextWord in v
        )

    modelTexts = getAllModelTexts()
    for match in matches:
        model, modelId = match
        instanceData = modelTexts[model][modelId]
        text = instanceData["text"]
        location = text.find(" " + wordsstr + " ")
        if location >= 0:
            preview = text[max(location - 50, 0) : min(location + 50, len(text))]
            preview = preview.replace(" " + wordsstr + " ", " <b>" + wordsstr + "</b> ")
            yield {
                "model": model,
                "id": modelId,
                "name": instanceData["name"],
                "preview": preview,
            }
