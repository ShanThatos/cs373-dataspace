from extensions import *
import sys
import json
import re
import pickle


"""
dict {
  <word>: dict{
    <model id>: dict {
      "frequency": <int frequency>,
      "succeeding-words": list [succeeding_words]
    }
     ...
  }
}
"""

drugsBank, compoundsBank, elementsBank = {}, {}, {}
allBanks = [drugsBank, compoundsBank, elementsBank]


def remakeWordBanks():
    compileDataIntoBanks()
    serializeAllBanks()


def loadWordBanks():
    log("Loading Word Bank...")
    return deserializeAllBanks()
    log("Word Bank Loaded!")


def compileDataIntoBanks():

    log("Getting all raw_text instances from database")
    data = executeRawSQL(f"SELECT * FROM media WHERE media_type = 'raw_text'")

    log("Inserting Data Into Data Structure...")

    def addToBank(bank, k, v):
        try:
            bank[k].update(v)
        except:
            bank[k] = v

    for media in data:

        modelID = media["model_id"]
        modelType = media["model_type"]
        text = media["media_data"].lower()

        words = re.split("\W+", text)
        # print("#DEBUG#", words)
        cache = set()

        for targetWord in words:
            if targetWord not in cache:
                # returns the SUCCEEDING words after each match
                succWords = re.findall(r"(?<=\b" + targetWord + "\s)(\w+)", text)

                firstOccurence = text.index(targetWord)

                ModelInstanceData = {
                    modelID: {
                        "freq": len(succWords),
                        "occurences": len(succWords),
                        "firstOcc": firstOccurence,
                    }
                }
                if modelType == "drugs":
                    addToBank(drugsBank, targetWord, ModelInstanceData)
                elif modelType == "compounds":
                    addToBank(compoundsBank, targetWord, ModelInstanceData)
                elif modelType == "elements":
                    addToBank(elementsBank, targetWord, ModelInstanceData)
                cache.add(targetWord)

        log("All words added from text media of " + modelType + ": " + str(modelID))

    log(
        "Total Words Added To All 3 Bank:",
        len(drugsBank) + len(compoundsBank) + len(elementsBank),
        "\n",
    )


def serializeAllBanks():
    log("Serializing From Word Bank To Picklefile")
    with open("./scraper/original/data/allBanksPickle.pkl", "wb") as pickleFile:
        pickle.dump(allBanks, pickleFile, -1)


def deserializeAllBanks():
    log("Deserializing From Picklefile And Returning Word Bank")
    with open("./scraper/original/data/allBanksPickle.pkl", "rb") as pickleFile:
        return pickle.load(pickleFile)
