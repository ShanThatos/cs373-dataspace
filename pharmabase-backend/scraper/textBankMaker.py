from extensions import *
import sys
import json
import re
import pickle


drugsTextBank, compoundsTextBank, elementsTextBank = {}, {}, {}
allTextBanks = [drugsTextBank, compoundsTextBank, elementsTextBank]


def remakeTextBanks():
    compileDataIntoBanks()
    serializeAllBanks()


def loadTextBanks():
    log("Loading Text Bank...")
    return deserializeAllBanks()


def compileDataIntoBanks():

    log("Getting all raw_text instances from database")
    data = executeRawSQL(f"SELECT * FROM media WHERE media_type = 'raw_text'")

    log("Inserting Data Into Data Structure...")

    def addToBank(bank, k, v):
        try:
            bank[k].update(v)
        except:
            bank[k] = v

    for media in data:

        modelID = media["model_id"]
        modelType = media["model_type"]
        modelText = media["media_data"].lower()

        modelName = executeRawSQL(
            f"SELECT name FROM {modelType} WHERE id = '{modelID}'"
        )
        modelName = modelName[0]["name"]

        ModelInstanceData = {
            "modelName": modelName,
            "modelType": modelType,
            "modelText": modelText,
        }
        if modelType == "drugs":
            addToBank(drugsTextBank, modelID, ModelInstanceData)
        elif modelType == "compounds":
            addToBank(compoundsTextBank, modelID, ModelInstanceData)
        elif modelType == "elements":
            addToBank(elementsTextBank, modelID, ModelInstanceData)

        log("Added Text Media for " + modelType + ": " + str(modelID))

    log(
        "Total Text Media Added",
        len(drugsTextBank) + len(compoundsTextBank) + len(elementsTextBank),
        "\n",
    )


def serializeAllBanks():
    log("Serializing From Text Bank To Picklefile")
    with open("./scraper/original/data/allTextBanksPickle.pkl", "wb") as pickleFile:
        pickle.dump(allTextBanks, pickleFile, -1)


def deserializeAllBanks():
    log("Deserializing From Picklefile And Returning Text Bank")
    with open("./scraper/original/data/allTextBanksPickle.pkl", "rb") as pickleFile:
        return pickle.load(pickleFile)
