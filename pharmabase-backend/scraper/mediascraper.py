import urllib.parse

from extensions import db, log, executeRawSQL, fetchJSON, writeToFile
from models import *


def mediaScrape():
    wikiScrapeAndSave()


def wikiScrapeAndSave():
    allData = []
    mainModels = [Drug, Compound, Element]
    modelInstances = [Drug.query.all(), Compound.query.all(), Element.query.all()]
    totalCount = sum(model.query.count() for model in mainModels)
    currentCount = 0

    for model, instances in zip(mainModels, modelInstances):
        model_type = model.__tablename__
        for instance in instances:
            model_id = instance.id
            name = instance.name
            log(f"Scraping {model_type}: {name} <{model_id}>")
            encodedName = urllib.parse.quote(name)
            try:
                res = fetchJSON(
                    f"https://en.wikipedia.org/w/api.php?format=json&action=opensearch&search={encodedName}"
                )
                wikiPageName = res[1][0]
                encodedPageName = urllib.parse.quote(wikiPageName)
                res = fetchJSON(
                    f"https://en.wikipedia.org/w/api.php?format=json&action=query&redirects=1&prop=extracts|pageimages&exintro=true&explaintext=true&piprop=original&titles={encodedPageName}"
                )

                data = res["query"]["pages"][list(res["query"]["pages"].keys())[0]]
                try:
                    text = data["extract"].strip()
                    if text:
                        log("Found text")
                        allData.append(
                            {
                                "model_type": model_type,
                                "model_id": model_id,
                                "media_src": "wikimain",
                                "media_type": "raw_text",
                                "media_data": text,
                            }
                        )
                except Exception as err:
                    print(
                        "Uh oh something bad happened - didn't find text extract for",
                        name,
                        "=",
                        wikiPageName,
                    )
                try:
                    imgUrl = data["original"]["source"]
                    if imgUrl:
                        log("Found image")
                        allData.append(
                            {
                                "model_type": model_type,
                                "model_id": model_id,
                                "media_src": "wikimain",
                                "media_type": "image_url",
                                "media_data": imgUrl,
                            }
                        )
                except Exception as err:
                    print(
                        "Uh oh something bad happened - didn't find thumbnail image for",
                        name,
                        "=",
                        wikiPageName,
                    )

                res = fetchJSON(
                    f"https://en.wikipedia.org/w/api.php?format=json&action=query&redirects=1&prop=images&imlimit=100&titles={encodedPageName}"
                )
                data = res["query"]["pages"][list(res["query"]["pages"].keys())[0]]
                try:
                    titleChecks = [
                        x for x in wikiPageName.lower().split(" ") if len(x) > 5
                    ]
                    if not titleChecks:
                        titleChecks = wikiPageName.lower().split(" ")
                    # crappy attempt at filtering out only relevant images^ with keywords from wiki page name

                    goodImages = [
                        x["title"]
                        for x in data["images"]
                        if any(y in x["title"].lower() for y in titleChecks)
                        and any(
                            x["title"].lower().endswith(y)
                            for y in [".jpeg", ".jpg", ".png", ".gif"]
                        )
                    ][:5]
                    titlesParam = "|".join(goodImages)
                    res = fetchJSON(
                        f"https://en.wikipedia.org/w/api.php?format=json&action=query&redirects=1&prop=imageinfo&iiprop=url&titles={titlesParam}"
                    )
                    data = list(res["query"]["pages"].values())
                    for pg in data:
                        try:
                            imgUrl = pg["imageinfo"][0]["url"]
                            log("Found extra image")
                            allData.append(
                                {
                                    "model_type": model_type,
                                    "model_id": model_id,
                                    "media_src": "wikilame",
                                    "media_type": "image_url",
                                    "media_data": imgUrl,
                                }
                            )
                        except Exception as err:
                            print("Skipping extra image :(")
                except Exception as err:
                    print(
                        "Welp something happened here -- extra image gathering",
                        wikiPageName,
                    )
            except Exception as err:
                print("Uh oh something bad happened --", name)

            log(f"{currentCount * 100 / totalCount:.02f}% done with scraping")
            currentCount += 1

    writeToFile("./scraper/original/data/wikiMediaData.json", allData)


def mediaInsert():
    log("Dropping Media table...")
    executeRawSQL(f"DROP TABLE IF EXISTS {Media.__tablename__}", expectData=False)
    log("Creating Media table...")
    Media.__table__.create(db.session.bind)

    for f in [insertElement3DMedia, insertCompoundPubchemImages, insertWikiMedia]:
        f()


def insertElement3DMedia():
    log("Inserting Element 3D Media Links...")

    linkFormat = "https://storage.googleapis.com/search-ar-edu/periodic-table/element_[ANUM]_[ENAME]/element_[ANUM]_[ENAME].glb"
    elements = Element.query.all()
    mediaObjects = []
    for el in elements:
        el: Element
        media = Media()
        media.model_type = Element.__tablename__
        media.model_id = el.id
        media.media_src = "google-elements"
        media.media_type = "glb_url"
        link = linkFormat.replace("[ANUM]", f"{el.atomic_num:03}").replace(
            "[ENAME]", str(el.name).lower()
        )
        media.media_data = link

        mediaObjects.append(media)

    db.session.bulk_save_objects(mediaObjects)
    db.session.commit()
    log(f"Committed {len(mediaObjects)} element 3d media objects...")


def insertCompoundPubchemImages():
    log("Inserting Compound PubChem Image Media...")

    linkFormat = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/[CNAME]/png"
    compounds = Compound.query.all()
    mediaObjects = []
    for cmp in compounds:
        cmp: Compound
        media = Media()
        media.model_type = Compound.__tablename__
        media.model_id = cmp.id
        media.media_src = "pubchem"
        media.media_type = "image_url"
        link = linkFormat.replace("[CNAME]", urllib.parse.quote(cmp.name))
        media.media_data = link

        mediaObjects.append(media)

    db.session.bulk_save_objects(mediaObjects)
    db.session.commit()
    log(f"Committed {len(mediaObjects)} compound media link objects...")


def insertWikiMedia():
    log("Loading Wikipedia Media Data...")
    with open("./scraper/original/data/wikiMediaData.json", "r") as dataFile:
        data = json.load(dataFile)

        log("Inserting media...")

        totalCommitted = [0]
        bulkMedia = []

        def commitBulk():
            db.session.bulk_save_objects(bulkMedia)
            totalCommitted[0] += len(bulkMedia)
            bulkMedia.clear()
            log(totalCommitted[0], "total wikimedia committed")

        for mediaData in data:
            bulkMedia.append(Media(**mediaData))
            if len(bulkMedia) == 2000:
                commitBulk()
        if bulkMedia:
            commitBulk()
        db.session.commit()

        log("Finished committing wikimedia")
