import os
import requests
import json
import random


def fetchJSON(url: str):
    response = requests.get(url)
    return response.json()


def writeToFile(fileName: str, data, indent=4):
    os.makedirs(os.path.dirname(fileName), exist_ok=True)
    json.dump(data, open(fileName, "w"), indent=indent)


manufacturers = set()
ingredients = set()

limit = 500
for i in range(0, 25001, limit):
    print(i)
    data = fetchJSON(f"https://api.fda.gov/drug/drugsfda.json?skip={i}&limit={limit}")
    for x in data["results"]:
        if "openfda" in x and "manufacturer_name" in x["openfda"]:
            manufacturers.update(x["openfda"]["manufacturer_name"])
        if "products" in x:
            for product in x["products"]:
                for ai in product["active_ingredients"]:
                    aiName = ai["name"]
                    ingredients.add(aiName)

print(len(ingredients))
