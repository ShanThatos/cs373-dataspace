import os
import requests
import json
import random


def fetchJSON(url: str):
    response = requests.get(url)
    return response.json()


def writeToFile(fileName: str, data, indent=4):
    os.makedirs(os.path.dirname(fileName), exist_ok=True)
    json.dump(data, open(fileName, "w"), indent=indent)


# solaire
def solaireScrape():
    print("Collecting Data from Solaire...")

    def getBodiesData():
        data = fetchJSON("https://api.le-systeme-solaire.net/rest/bodies/")
        return data

    def filterBodyTypes(
        data,
        allowedBodyTypes=[
            "Star",
            "Planet",
            "Dwarf Planet",
            "Asteroid",
            "Comet",
            "Moon",
        ],
    ):
        return [x for x in data["bodies"] if x["bodyType"] in allowedBodyTypes]

    allData = getBodiesData()
    writeToFile("./data/solaireData.json", allData)

    planets = filterBodyTypes(allData, ["Planet", "Dwarf Planet"])
    writeToFile("./data/solairePlanets.json", planets)

    moons = filterBodyTypes(allData, ["Moon"])
    writeToFile("./data/solaireMoons.json", moons)

    print("Finished Solaire...")


def jplScrape():
    print("Collecting Data from JPL...")

    def getData(numBodies: int, rng: random.Random = random.Random(42)):
        usedIds = set()
        data = []
        while (dlen := len(data)) < numBodies:
            if dlen % (numBodies // 10) == 0:
                print("\t", dlen, "/", numBodies, "bodies recieved")
            randId = 0
            while (randId := rng.randint(1, 610000)) in usedIds:
                pass
            assert 1 <= randId <= 610000
            bodyData = fetchJSON(f"https://ssd-api.jpl.nasa.gov/sbdb.api?spk=2{randId}")
            data.append(bodyData)

        return data

    allData = getData(1000)
    writeToFile("./data/jplData.json", allData)

    print("Finished JPL...")


def nasaImageScrape():
    data = json.load(open("./data/solairePlanets.json"))
    for pd in data:
        fetchJSON(
            f"https://images-api.nasa.gov/search?title={pd['id']}&media_type=image"
        )


# Future
# try to grab jpl data from multiple orbit classes
# jpl data doesn't have everything we may need

if __name__ == "__main__":
    solaireScrape()
    jplScrape()
