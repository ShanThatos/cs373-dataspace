import datetime
import json

from functools import lru_cache

from sqlalchemy import (
    Column,
    Date,
    Float,
    ForeignKey,
    Integer,
    Numeric,
    Text,
    UniqueConstraint,
    inspect,
)
from sqlalchemy.orm import relationship

from extensions import db, executeRawSQL, log


class Element(db.Model):
    __tablename__ = "elements"

    id = Column(Integer, primary_key=True)

    # ---SEARCHABLE---
    # Generic element name
    name = Column(Text)
    # Element Symbol
    symbol = Column(Text)
    # Atomic Number
    atomic_num = Column(Integer)
    # Standard State (solid, liquid or gas under standard conditions)
    standard_state = Column(Text)
    # Element Classification (alkali, noble gas, etc...)
    group_block = Column(Text)

    # ---FILTERABLE---
    # Atomic Radius
    atomic_radius = Column(Integer)
    # Boiling Point
    boiling_point = Column(Integer)
    # Year Discovered
    year_discovered = Column(
        Integer
    )  # some are 'Ancient' -- we are setting Ancient to 0
    # Ion Radius
    atomic_mass = Column(Float)
    # Ionization Energy
    ionization_en = Column(Integer)

    drugs = relationship(
        "Drug", back_populates="elements", secondary="drugelement", lazy="raise"
    )
    compounds = relationship(
        "Compound", back_populates="elements", secondary="compoundelement", lazy="raise"
    )

    def getSearchableColumns():
        return ["id", "name", "symbol", "atomic_num", "standard_state", "group_block"]


class Compound(db.Model):
    __tablename__ = "compounds"

    id = Column(Integer, primary_key=True)

    # ---SEARCHABLE---
    # Compound name (linked to openFDA drug)
    name = Column(Text)
    # IUPAC name (Preferred)
    iupac_name = Column(Text)
    # IUPAC International Chemical Identifier
    iupac_inchi = Column(Text)
    # InChI key
    iupac_inchi_key = Column(Text)
    # Molecular Formula
    molecular_formula = Column(Text)

    # ---FILTERABLE---
    # Weight of compound (similar to exact mass)
    molecular_weight = Column(Float)
    # Number of atoms (non-hydrogen)
    heavy_atom_count = Column(Numeric)
    # Hydrogen bond donor count
    hydrogen_bond_donor_count = Column(Numeric)
    # hydrogen bond acceptor count
    hydrogen_bond_acceptor_count = Column(Numeric)
    # number of bonds that allow free rotations around themselves
    rotatable_bond_count = Column(Numeric)

    drugs = relationship(
        "Drug", back_populates="compounds", secondary="drugcompound", lazy="raise"
    )
    elements = relationship(
        "Element", back_populates="compounds", secondary="compoundelement", lazy="raise"
    )

    def getSearchableColumns():
        return [
            "id",
            "name",
            "iupac_name",
            "iupac_inchi",
            "iupac_inchi_key",
            "molecular_formula",
        ]


class Drug(db.Model):
    __tablename__ = "drugs"

    # Additional column we will have for every model (PostgreSQL is happy when we do this)
    # by default this autoincrements
    id = Column(Integer, primary_key=True)
    # Drug name
    name = Column(Text)
    # SPL_ID
    spl_id = Column(Text)
    # Marketing Status
    marketing_status = Column(Text)
    # Dosage Form
    dosage_form = Column(Text)
    # Submission Dates
    first_submission_date = Column(Date)
    last_submission_date = Column(Date)
    # Product Type -- OTC, prescriptions, etc
    product_type = Column(Text)
    # Route of administration -- oral, intravenous, etc
    route_of_administration = Column(Text)
    # Brand Name
    brand_name = Column(Text)
    # Manufacturer Name
    manufacturer_name = Column(Text)

    # client suggestion
    # Pharm Class PE (Purpose)
    purpose = Column(Text)

    compounds = relationship(
        "Compound", back_populates="drugs", secondary="drugcompound", lazy="raise"
    )
    elements = relationship(
        "Element", back_populates="drugs", secondary="drugelement", lazy="raise"
    )

    def getSearchableColumns():
        return [
            "id",
            "name",
            "spl_id",
            "manufacturer_name",
            "brand_name",
            "route_of_administration",
        ]


# Many-to-many relationship tables
class DrugCompound(db.Model):
    __tablename__ = "drugcompound"
    id = Column(Integer, primary_key=True)
    drugs_id = Column(
        Integer, ForeignKey("drugs.id", ondelete="CASCADE"), nullable=False
    )
    compounds_id = Column(
        Integer, ForeignKey("compounds.id", ondelete="CASCADE"), nullable=False
    )
    __table_args__ = (
        UniqueConstraint("drugs_id", "compounds_id", name="dc_constraint"),
    )


class CompoundElement(db.Model):
    __tablename__ = "compoundelement"
    id = Column(Integer, primary_key=True)
    compounds_id = Column(
        Integer, ForeignKey("compounds.id", ondelete="CASCADE"), nullable=False
    )
    elements_id = Column(
        Integer, ForeignKey("elements.id", ondelete="CASCADE"), nullable=False
    )
    __table_args__ = (
        UniqueConstraint("compounds_id", "elements_id", name="ce_constraint"),
    )


class DrugElement(db.Model):
    __tablename__ = "drugelement"
    id = Column(Integer, primary_key=True)
    drugs_id = Column(
        Integer, ForeignKey("drugs.id", ondelete="CASCADE"), nullable=False
    )
    elements_id = Column(
        Integer, ForeignKey("elements.id", ondelete="CASCADE"), nullable=False
    )
    __table_args__ = (
        UniqueConstraint("drugs_id", "elements_id", name="de_constraint"),
    )


class Media(db.Model):
    __tablename__ = "media"
    id = Column(Integer, primary_key=True)

    # model_type is one of drugs, compounds, elements (the original table names)
    model_type = Column(Text, nullable=False)
    model_id = Column(Integer, nullable=False)

    # media_src is one of wikimain, wikilame, pubchem, google-elements (more might be added later)
    # wikimain is the core media -- the main text of a page & the main image
    # wikilame is the extra media -- possible some more images
    media_src = Column(Text, nullable=False)
    # media_type is one of raw_text, raw_html, image_url, glb_url
    media_type = Column(Text, nullable=False)
    media_data = Column(Text, nullable=False)


# /api/media?filters=[["model_type", "eq", "drug"], ["model_id", "eq", 7], ["media_type", "eq", "image_url"]]

ALL_MODELS = [
    Drug,
    Compound,
    Element,
    DrugCompound,
    CompoundElement,
    DrugElement,
    Media,
]

CONNECTED_MODEL_MAP = {
    "drugs-compounds": DrugCompound,
    "compounds-drugs": DrugCompound,
    "drugs-elements": DrugElement,
    "elements-drugs": DrugElement,
    "compounds-elements": CompoundElement,
    "elements-compounds": CompoundElement,
}


DROPDOWN_FIELDS = {
    "drugs": [
        "marketing_status",
        "dosage_form",
        "product_type",
        "route_of_administration",
    ],
    "compounds": [],
    "elements": ["group_block", "standard_state"],
}
FILTER_TYPES = {
    "TEXT": "searchbox",
    "INTEGER": "range_number",
    "NUMERIC": "range_number",
    "FLOAT": "range_number",
    "DATE": "range_date",
}


@lru_cache
def getModelSchema(model):
    if isinstance(model, str):
        model = next(x for x in ALL_MODELS if x.__tablename__ == model)

    def fieldData(field):
        if field.name in DROPDOWN_FIELDS.get(model.__tablename__, []):
            vals = executeRawSQL(
                f"SELECT DISTINCT {field.name} AS val FROM {model.__tablename__}"
            )
            vals = [x["val"] for x in vals]
            return {"type": str(field.type), "filter_type": "dropdown", "values": vals}
        return {
            "type": str(field.type),
            "filter_type": FILTER_TYPES.get(str(field.type), "searchbox"),
        }

    return {x.name: fieldData(x) for x in inspect(model).c}


drugCompoundConnections = set()
compoundElementConnections = set()


def insertAllData():

    global compoundElementConnections
    compoundElementConnections = set()

    global drugCompoundConnections
    drugCompoundConnections = set()

    insertDrugData()
    insertCompoundData()
    insertElementData()

    # CONNECTIONS
    # Note that insertCompoundElementData() MUST be
    # ran first because insertDrugCompoundData() depends
    # on the former

    insertCompoundElementData()
    insertDrugCompoundData()
    insertDrugElementData()


def remakeDatabase():
    log("Remaking DB")

    log("Dropping all tables...")
    db.drop_all()
    log("Creating all tables...")
    db.create_all()

    insertAllData()


def insertCompoundData():
    log("Loading Compounds...")
    with open("./scraper/original/data/pubchemCompoundData.json", "r") as dataFile:
        data = json.load(dataFile)

        log("Inserting Compounds...")

        totalCommitted = [0]
        bulkCompounds = []

        def commitBulk():
            db.session.bulk_save_objects(bulkCompounds)
            totalCommitted[0] += len(bulkCompounds)
            bulkCompounds.clear()
            log(totalCommitted[0], "total compounds committed")

        for compoundData in data:

            compound = Compound()

            # name
            compound.name = compoundData["name"]

            elementsContained = compoundData["data"]["PC_Compounds"][0]["atoms"][
                "element"
            ]
            for ele in elementsContained:
                compoundElementConnections.add((compound.name, ele))
            # heavy atoms
            compound.heavy_atom_count = compoundData["data"]["PC_Compounds"][0][
                "count"
            ]["heavy_atom"]

            props = compoundData["data"]["PC_Compounds"][0]["props"]

            # could be more efficient with the use of an iterator but we're not make the db often
            # so I opted for more readable code. Can't index directly due to format of JSON
            for urn_val_pair in props:
                urn = urn_val_pair["urn"]
                # hydrogen bond acceptor
                if urn["label"] == "Count" and urn["name"] == "Hydrogen Bond Acceptor":
                    compound.hydrogen_bond_acceptor_count = urn_val_pair["value"][
                        "ival"
                    ]
                # hydrogen bond donor
                elif urn["label"] == "Count" and urn["name"] == "Hydrogen Bond Donor":
                    compound.hydrogen_bond_donor_count = urn_val_pair["value"]["ival"]
                # Rotatable bond
                elif urn["label"] == "Count" and urn["name"] == "Rotatable Bond":
                    compound.rotatable_bond_count = urn_val_pair["value"]["ival"]
                # Preffered IUPAC name
                elif urn["label"] == "IUPAC Name" and urn["name"] == "Preferred":
                    compound.iupac_name = urn_val_pair["value"]["sval"]
                # InChI
                elif urn["label"] == "InChI":
                    compound.iupac_inchi = urn_val_pair["value"]["sval"]
                # InChI Key
                elif urn["label"] == "InChIKey":
                    compound.iupac_inchi_key = urn_val_pair["value"]["sval"]
                # Molecular Formula
                elif urn["label"] == "Molecular Formula":
                    compound.molecular_formula = urn_val_pair["value"]["sval"]
                # Molecular Weight
                elif urn["label"] == "Molecular Weight":
                    compound.molecular_weight = urn_val_pair["value"]["sval"]

            bulkCompounds.append(compound)
            if len(bulkCompounds) == 500:  # commit every 500 additions
                commitBulk()

        if bulkCompounds:  # commit any remaining additions
            commitBulk()
        db.session.commit()


def insertElementData():
    log("Loading Elements...")
    with open("./scraper/original/data/elementsdata.json", "r") as dataFile:
        data = json.load(dataFile)

        log("Inserting Elements...")

        totalCommitted = [0]  # sum total of elements committed to database
        bulkElements = []  # list of current elements to be committed in bulk

        def commitBulk():
            db.session.bulk_save_objects(bulkElements)
            totalCommitted[0] += len(bulkElements)
            bulkElements.clear()
            log(totalCommitted[0], "total elements committed")

        for elementData in data:

            # we are only interested in the elements that possess these key attributes
            req_fields = ["standardState", "atomicRadius", "boilingPoint"]
            if any((not elementData[x]) for x in req_fields):  # reject useless elements
                continue

            # set searchable
            element = Element()
            element.name = elementData["name"]
            element.symbol = elementData["symbol"]
            element.atomic_num = elementData["atomicNumber"]
            element.standard_state = elementData["standardState"]
            element.group_block = elementData["groupBlock"]

            # set filterable
            element.atomic_radius = elementData["atomicRadius"]
            element.boiling_point = elementData["boilingPoint"]
            element.year_discovered = (
                0
                if elementData["yearDiscovered"] == "Ancient"
                else elementData["yearDiscovered"]
            )
            element.ionization_en = elementData["ionizationEnergy"]

            # in some elements the value associated with atomic mass is a list with a single int (e.g [98])
            # in other cases it is a string representing a float, directly followed by a number in parenthesis (e.g "92.90638(2)")
            # we take this oddity into account here and extract the value accordingly
            element.atomic_mass = (
                elementData["atomicMass"][0]
                if isinstance(elementData["atomicMass"], list)
                else float(elementData["atomicMass"].split("(")[0])
            )
            bulkElements.append(element)

            if len(bulkElements) == 500:
                commitBulk()
        if bulkElements:  # commit remaining elements if any
            commitBulk()
        db.session.commit()


def insertDrugData():
    global drugCompoundConnections
    log("Loading Drugs...")
    with open("./scraper/original/data/fdaDrugData.json", "r") as dataFile:
        data = json.load(dataFile)

        log("Inserting Drugs...")

        totalCommitted = [0]
        bulkDrugs = []

        def commitBulk():
            db.session.bulk_save_objects(bulkDrugs)
            totalCommitted[0] += len(bulkDrugs)
            bulkDrugs.clear()
            log(totalCommitted[0], "total drugs committed")

        for drugData in data:
            # This could be a problem, we need data from openfda tag that only about 40% of the drugs have
            if any(
                (x not in drugData or not drugData[x])
                for x in ["openfda", "products", "submissions"]
            ):
                continue
            openfda = drugData["openfda"]
            if any(
                (x not in openfda or not openfda[x])
                for x in ["generic_name", "spl_id", "product_type", "manufacturer_name"]
            ):
                continue
            product = drugData["products"][0]
            if any(
                (x not in product or not product[x])
                for x in ["marketing_status", "dosage_form", "route", "brand_name"]
            ):
                continue
            if any("submission_status_date" not in x for x in drugData["submissions"]):
                continue

            drug = Drug()
            drug.name = openfda["generic_name"][0]
            drug.spl_id = openfda["spl_id"][0]
            drug.product_type = openfda["product_type"][0]
            drug.manufacturer_name = openfda["manufacturer_name"][0]

            drug.marketing_status = product["marketing_status"]
            drug.dosage_form = product["dosage_form"]
            drug.route_of_administration = product["route"]
            drug.brand_name = product["brand_name"]

            submissionDates = (
                x["submission_status_date"] for x in drugData["submissions"]
            )
            submissionDates = [
                datetime.date(int(x[:4]), int(x[4:6]), int(x[6:8]))
                for x in submissionDates
            ]
            drug.first_submission_date = min(submissionDates)
            drug.last_submission_date = max(submissionDates)

            drug.purpose = ", ".join(openfda.get("pharm_class_pe", []))

            for ing in product["active_ingredients"]:
                drugCompoundConnections.add((drug.spl_id, ing["name"]))
                # just decided to use spl_id as a drug unique identifier for later

            bulkDrugs.append(drug)
            if len(bulkDrugs) == 500:
                commitBulk()
        if bulkDrugs:
            commitBulk()
        db.session.commit()


def insertCompoundElementData():
    global compoundElementConnections

    totalCommitted = [0]
    bulkCE = []

    def commitBulk():
        db.session.bulk_save_objects(bulkCE)
        totalCommitted[0] += len(bulkCE)
        bulkCE.clear()
        log(totalCommitted[0], "total CE connections committed")

    log("Loading All Compounds and Elements...")
    allCompounds = Compound.query.all()
    allElements = Element.query.all()

    log("Hashing All Compounds & Elements...")

    allCompoundsMap = {x.name: x.id for x in allCompounds}
    allElementsMap = {x.atomic_num: x.id for x in allElements}
    log("Inserting Compound Element Connections...")

    for compoundName, atomicNum in compoundElementConnections:
        compounds_id = allCompoundsMap.get(compoundName, None)
        elements_id = allElementsMap.get(atomicNum, None)
        # print("compounds_id:", compounds_id, " | elements_id:", elements_id)
        if compounds_id is not None and elements_id is not None:  # found connection
            ce = CompoundElement(compounds_id=compounds_id, elements_id=elements_id)
            bulkCE.append(ce)
            if len(bulkCE) == 500:
                commitBulk()
    if bulkCE:
        commitBulk()
    db.session.commit()


def insertDrugCompoundData():
    global drugCompoundConnections

    totalDCCommitted = [0]
    bulkDC = []

    def commitDCBulk():
        db.session.bulk_save_objects(bulkDC)
        totalDCCommitted[0] += len(bulkDC)
        bulkDC.clear()
        log(totalDCCommitted[0], "total DC connections committed")

    log("Loading All Drugs, Compounds...")
    allDrugs = Drug.query.all()
    allCompounds = Compound.query.all()

    log("Hashing All Drugs & Compounds...")

    allDrugsMap = {x.spl_id: x.id for x in allDrugs}
    allCompoundsMap = {x.name: x.id for x in allCompounds}

    log("Inserting Drug Compound Connections...")

    for drugSplID, compoundName in drugCompoundConnections:
        drugs_id = allDrugsMap.get(drugSplID, None)
        compounds_id = allCompoundsMap.get(compoundName, None)
        if drugs_id is not None and compounds_id is not None:
            dc = DrugCompound(drugs_id=drugs_id, compounds_id=compounds_id)
            bulkDC.append(dc)
            if len(bulkDC) == 500:
                commitDCBulk()

    if bulkDC:
        commitDCBulk()
    db.session.commit()


def insertDrugElementData():
    log("Fun SQL query for generating drug elements table")
    executeRawSQL(
        """
           INSERT INTO drugelement (drugs_id, elements_id)
               SELECT DISTINCT dc.drugs_id, ce.elements_id
                   FROM drugcompound dc, compoundelement ce
                   WHERE dc.compounds_id = ce.compounds_id
       """,
        expectData=False,
    )
    count = executeRawSQL("SELECT COUNT(*) FROM drugelement")[0]["count"]
    log(f"Inserted {count} DE connections")


def clearDisconnectedData():
    for proc in CLEAR_PROCS:
        executeRawSQL(proc, expectData=False)


import codecs

import sadisplay


def generateUML():
    desc = sadisplay.describe(ALL_MODELS)
    with codecs.open("schema.plantuml", "w", encoding="utf-8") as f:
        f.write(sadisplay.plantuml(desc))
    print("Run 'java -jar plantuml.jar schema.plantuml'")


CLEAR_PROCS = [
    x.strip()
    for x in """
DELETE FROM drugs d
   WHERE   NOT EXISTS(SELECT * FROM drugcompound dc WHERE dc.drugs_id = d.id)
   OR      NOT EXISTS(SELECT * FROM drugelement de WHERE de.drugs_id = d.id);
 
DELETE FROM compounds c
   WHERE   NOT EXISTS(SELECT * FROM drugcompound dc WHERE dc.compounds_id = c.id)
   OR      NOT EXISTS(SELECT * FROM compoundelement ce WHERE ce.compounds_id = c.id);
 
DELETE FROM elements e
   WHERE   NOT EXISTS(SELECT * FROM compoundelement ce WHERE ce.elements_id = e.id)
   OR      NOT EXISTS(SELECT * FROM drugelement de WHERE de.elements_id = e.id);
""".replace(
        "\n", " "
    ).split(
        ";"
    )
    if x.strip()
]
